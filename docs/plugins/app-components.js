/* eslint-disable import/first */
/* eslint-disable import/order */
/* eslint-disable no-unused-vars */
import Vue from 'vue'
import { version } from '../package.json'
// import '@ozon-erp/ui-lib/styles/config.scss'

/** Full */
// import UI from '@ozon-erp/ui-lib'
// Vue.use(UI)

/** Treeshakable */
import * as components from '@ozon-erp/ui-lib/components'
import '@ozon-erp/ui-lib/directives'
// console.log(directives)

// Helper for each
function each(obj, callback) {
  Object.keys(obj).forEach(key => callback(obj[key], key))
}

const kit = {
  version,
  install(Vue, options = {}) {
    each(components, (component, name) => Vue.component(name, component))
    // each(directives, (directive, name) => Vue.directive(name, directive))
  }
}
Vue.use(kit)

// // Install by default if using the script tag
// if (typeof window !== 'undefined' && 'Vue' in window) {
//   Vue.use(kit, window.bootstrapForVueConfig || {})
// }

/** Components as plugins */
import { Modal, Message, Prompt } from '@ozon-erp/ui-lib/plugins'
Vue.use(Message, { delay: 3000, order: 'desc' })
Vue.use(Modal)
Vue.use(Prompt)

/** Inject plugins into vue and Nuxt context */
export default (ctx, inject) => {
  inject('message', Message)
  inject('modal', Modal)
  inject('prompt', Prompt)
}
