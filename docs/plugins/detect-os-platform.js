/**
 * Получае платформу (операционную систему)
 * `Windows` все версии Windows
 * `MacOS` все версии Macintosh OS
 * `Linux` все версии Linux
 */

const platforms = ['Win', 'Mac', 'Linux']
const detect = platforms.filter(os => navigator.appVersion.includes(os))

document.body.classList.add(detect.toString().toLowerCase())
