export default (ctx, inject) => {
  // app.router.afterEach(() => {
  //   if (store.state.menu.open) {
  //     setTimeout(() => store.commit('menu/close'), 10)
  //   }
  // })
  inject('PRODUCTION', ctx.env.PRODUCTION)
  inject('SITE_NAME', ctx.env.SITE_NAME)
  inject('VERSION', ctx.env.VERSION)
}
