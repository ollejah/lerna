[Nowto &rarr;](https://deliciousbrains.com/https-locally-without-browser-privacy-errors/)

### Creating a Self-Signed Certificate

```bash
openssl req -config cert.conf -new -sha256 -newkey rsa:2048 \
-nodes -keyout ssl.key -x509 -days 365 \
-out ssl.crt
```
