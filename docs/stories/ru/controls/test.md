---
title: Test
description: Песочница
order: 1

model: []
datalist: ['Github', 'Facebook', 'Twitter']
---

## Button test

<Button label="Button Label" color="primary" />
<Button label="Button Label" fill="primary" />
<Button label="Button Label" icon="check" />
<Button label="Button Label" icon="check" color="primary" />
<Button label="Button Label" icon="check" fill="primary" />
<Button icon="check" />
<Button icon="check" color="primary" />
<Button icon="check" fill="primary" />

## As Button Group

<box>
  <ButtonGroup>
    <Button>Первый</Button>
    <Button>Второй</Button>
    <Button>Третий</Button>
  </ButtonGroup>

  <ButtonGroup>
    <Button color="primary" fill>Первый</Button>
    <Button color="primary" fill>Второй</Button>
    <Button color="primary" fill>Третий</Button>
  </ButtonGroup>
</box>

<box>
  <CheckboxGroup v-model="model" :labels="datalist" present="button-group" />
</box>

<box>
<Tabs type="group" present="button-group">
  <Tab title="Вкладка #1">
    <p>Контент #1</p>
  </Tab>

  <Tab title="Вкладка #2">
    <p>Контент #2</p>
  </Tab>

  <Tab title="Вкладка #3">
    <p>Контент #3</p>
  </Tab>
</Tabs>
</box>

## Live Button

```vue live
<Button color="primary">Test This Buttton</Button>
```

## Live Alert

`type: 'error|info'`

```vue live
<Alert type="info">
  You are running Vue in development mode. Make sure to turn on production mode
  when deploying for production.
  <a href="https://vuejs.org/guide/deployment.html" target="_blank">
    See more tips
  </a>
</Alert>
```

## Code HTML

```html|nuxt.config.js
You are running Vue in development mode. Make sure to turn on production mode
when deploying for production.
<a href="https://vuejs.org/guide/deployment.html" target="_blank">
  See more tips
</a>
```

## Code JS

```js
label: {
  type: String,
  default: undefined
},
color: {
  type: String,
  default: undefined
}
```
