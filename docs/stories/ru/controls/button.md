---
title: Button
category: components
# click: e => console.log(`click`)
---

## Props

| Name    | Type    | Default   | Description                                                              |
| ------- | ------- | --------- | ------------------------------------------------------------------------ |
| label   | String  | undefined | Устанавливает текст кнопки. Также можно просто передать слот.            |
| color   | String  | undefined | Устанавливает цвет текста. [пример](#базовые-цвета)                      |
| fill    | String  | undefined | Устанавливает цвет фона и инвертирует цвет текста. [пример](#с-фоном)    |
| size    | String  | undefined | Устанавливает размер. [пример](#размеры)                                 |
| ghost   | Boolean | false     | Устанавливает прозрачный фон [пример](#прозрачные)                       |
| circle  | Boolean | false     | Делает кнопку с закругленными углами [пример](#цветные-закругленные)     |
| block   | Boolean | false     | Устанавливает ширину в размер контейнера [пример](#широкая)              |
| caps    | Boolean | false     | Устанавливает начертание шрифта `all-small-caps` [пример](#текст-капсом) |
| icon    | String  | undefined | Устанавливает иконку. [пример](/design-tokens/icons)                        |
| loading | Boolean | false     | Показывает прелоадер [пример](#анимация)                                 |

Также можно передавать [обычные атрибуты](https://developer.mozilla.org/ru/docs/Web/HTML/Element/Input)

**Пример**

`<Button>Button Label</Button>` или `<Button label="Button Label" />`

```vue live
<Button icon="download" color="primary" label="Кнопка с иконкой" />
```

<!-- <Button
  icon="download"
  color="primary"
  label="Кнопка с иконкой"
  v-on:click="$parent.$parent.click()"
/> -->

## Базовые цвета

<div>
  <Button color="primary" label="Button primary" />
  <Button color="secondary" label="Button secondary" />
  <Button color="success" label="Button success" />
  <Button color="warning" label="Button warning" />
  <Button color="error" disabled label="Button disabled" />
</div>

```vue
<Button color="primary" label="Button primary" />
<Button color="secondary" label="Button secondary" />
<Button color="success" label="Button success" />
<Button color="warning" label="Button warning" />
<Button color="error" disabled label="Button disabled" />
```

## С фоном

<div>
  <Button fill="primary" label="Button primary" />
  <Button fill="secondary" label="Button secondary" />
  <Button fill="success" label="Button success" />
  <Button fill="warning" label="Button warning" />
  <Button fill="error" label="Button error" />
</div>

```vue
<Button fill="primary" label="Button primary" />
<Button fill="secondary" label="Button secondary" />
<Button fill="success" label="Button success" />
<Button fill="warning" label="Button warning" />
<Button fill="error" label="Button error" />
```

## Прозрачные

### На светлом фоне

<div>
  <Button ghost label="Button" />
  <Button color="primary" ghost label="Button primary" />
  <Button color="secondary" ghost label="Button secondary" />
  <Button color="success" ghost label="Button success" />
  <Button color="warning" ghost label="Button warning" />
  <Button color="error" ghost label="Button error" />
</div>

```vue
<Button ghost label="Button" />
<Button color="primary" ghost label="Button primary" />
<Button color="secondary" ghost label="Button secondary" />
<Button color="success" ghost label="Button success" />
<Button color="warning" ghost label="Button warning" />
<Button color="error" ghost label="Button error" />
```

### На темном фоне

Добавьте свойство `dark-mode` кнопке или переключитесь в темный режим интерфейса

<div style="background-color: var(--dark); padding: 1rem">
  <Button ghost dark-mode label="Button" />
  <Button color="primary" ghost dark-mode label="Button primary" />
  <Button color="secondary" ghost dark-mode label="Button secondary" />
  <Button color="success" ghost dark-mode label="Button success" />
  <Button color="warning" ghost dark-mode label="Button warning" />
  <Button color="error" ghost dark-mode label="Button error" />
</div>

```vue
<Button ghost dark-mode label="Button" />
<Button color="primary" ghost dark-mode label="Button primary" />
<Button color="secondary" ghost dark-mode label="Button secondary" />
<Button color="success" ghost dark-mode label="Button success" />
<Button color="warning" ghost dark-mode label="Button warning" />
<Button color="error" ghost dark-mode label="Button error" />
```

## Закругленные

<div>
  <Button color="primary" circle label="Button primary" />
  <Button color="secondary" circle label="Button secondary" />
  <Button color="success" circle label="Button success" />
  <Button color="warning" circle label="Button warning" />
  <Button color="error" circle label="Button error" />
</div>

```vue
<Button color="primary" circle label="Button primary" />
<Button color="secondary" circle label="Button secondary" />
<Button color="success" circle label="Button success" />
<Button color="warning" circle label="Button warning" />
<Button color="error" circle label="Button error" />
```

<div>
  <Button fill="primary" circle label="Button primary" />
  <Button fill="secondary" circle label="Button secondary" />
  <Button fill="success" circle label="Button success" />
  <Button fill="warning" circle label="Button warning" />
  <Button fill="error" circle label="Button error" />
</div>

```vue
<Button fill="primary" circle label="Button primary" />
<Button fill="secondary" circle label="Button secondary" />
<Button fill="success" circle label="Button success" />
<Button fill="warning" circle label="Button warning" />
<Button fill="error" circle label="Button error" />
```

## С иконокой

<div>
  <Button icon="check" label="Button check" />
  <Button icon="add" label="Button add" />
  <Button icon="delete" label="Button delete" />
  <Button icon="remove" label="Button remove" />
  <Button icon="download" label="Button download" />
</div>

```vue
<Button icon="check" label="Button check" />
<Button icon="add" label="Button add" />
<Button icon="delete" label="Button delete" />
<Button icon="remove" label="Button remove" />
<Button icon="download" label="Button download" />
```

### Цветные

<div>
  <Button icon="check" color="primary" label="Button check" />
  <Button icon="add" color="secondary" label="Button add" />
  <Button icon="delete" color="success" label="Button delete" />
  <Button icon="remove" color="warning" label="Button remove" />
  <Button icon="download" color="error" label="Button download" />
</div>

```vue
<Button icon="check" color="primary" label="Button check" />
<Button icon="add" color="secondary" label="Button add" />
<Button icon="delete" color="success" label="Button delete" />
<Button icon="remove" color="warning" label="Button remove" />
<Button icon="download" color="error" label="Button download" />
```

### Цветные с фоном

<div>
  <Button icon="check" fill="primary" label="Button check" />
  <Button icon="add" fill="secondary" label="Button add" />
  <Button icon="delete" fill="success" label="Button delete" />
  <Button icon="remove" fill="warning" label="Button remove" />
  <Button icon="download" fill="error" label="Button download" />
</div>

```vue
<Button icon="check" fill="primary" label="Button check" />
<Button icon="add" fill="secondary" label="Button add" />
<Button icon="delete" fill="success" label="Button delete" />
<Button icon="remove" fill="warning" label="Button remove" />
<Button icon="download" fill="error" label="Button download" />
```

### Без текста

<div>
  <Button icon="check" color="primary" />
  <Button icon="add" color="secondary" />
  <Button icon="delete" color="success" />
  <Button icon="remove" color="warning" />
  <Button icon="download" color="error" />
</div>

```vue
<Button icon="check" color="primary" />
<Button icon="add" color="secondary" />
<Button icon="delete" color="success" />
<Button icon="remove" color="warning" />
<Button icon="download" color="error" />
```

### Цветные закругленные

<div>
  <Button icon="check" color="primary" circle label="Button check" />
  <Button icon="add" color="secondary" circle label="Button add" />
  <Button icon="delete" color="success" circle label="Button delete" />
  <Button icon="remove" color="warning" circle label="Button remove" />
  <Button icon="download" color="error" circle label="Button download" />
</div>

```vue
<Button icon="check" color="primary" circle label="Button check" />
<Button icon="add" color="secondary" circle label="Button add" />
<Button icon="delete" color="success" circle label="Button delete" />
<Button icon="remove" color="warning" circle label="Button remove" />
<Button icon="download" color="error" circle label="Button download" />
```

### Без текста закругленные

<div>
  <Button icon="check" color="primary" circle />
  <Button icon="add" color="secondary" circle />
  <Button icon="delete" color="success" circle />
  <Button icon="remove" color="warning" circle />
  <Button icon="download" color="error" circle />
</div>

```vue
<Button icon="check" color="primary" circle />
<Button icon="add" color="secondary" circle />
<Button icon="delete" color="success" circle />
<Button icon="remove" color="warning" circle />
<Button icon="download" color="error" circle />
```

### Без текста закругленные с фоном

<div>
  <Button icon="check" fill="primary" circle />
  <Button icon="add" fill="secondary" circle />
  <Button icon="delete" fill="success" circle />
  <Button icon="remove" fill="warning" circle />
  <Button icon="download" fill="error" circle />
</div>

```vue
<Button icon="check" fill="primary" circle />
<Button icon="add" fill="secondary" circle />
<Button icon="delete" fill="success" circle />
<Button icon="remove" fill="warning" circle />
<Button icon="download" fill="error" circle />
```

### Текст капсом

Установите `caps` для начертания шрифта `all-small-caps`

<div>
  <Button color="primary" caps label="Button primary" />
  <Button color="secondary" caps label="Button secondary" />
  <Button color="success" caps label="Button success" />
  <Button color="warning" caps label="Button warning" />
  <Button color="error" caps label="Button error" />
</div>

```vue
<Button color="primary" caps label="Button primary" />
<Button color="secondary" caps label="Button secondary" />
<Button color="success" caps label="Button success" />
<Button color="warning" caps label="Button warning" />
<Button color="error" caps label="Button error" />
```

## Размеры

> Доступны `xl, lg, sm, xs`. Размеры устанавливаются относительно `<body>`, не в `rem`, а в `em`. В нашем случаем размер текста у `<body>` равен 14px, от него и рассчитываем. Избегайте устанавливать размер шрифта родительскому контейнеру, в который помещается копмонент, в этом случае его размер будет рассчитываться от размера шрифта контейнера.

### Базовые размеры

<div>
  <Button size="xs" label="Button xs" />
  <Button size="sm" label="Button sm" />
  <Button label="Button default" />
  <Button size="lg" label="Button lg" />
  <Button size="xl" label="Button xl" />
</div>

```vue
<Button size="xs" label="Button xs" />
<Button size="sm" label="Button sm" />
<Button label="Button default" />
<Button size="lg" label="Button lg" />
<Button size="xl" label="Button xl" />
```

<div>
  <Button fill="primary" size="xs" label="Button xs" />
  <Button fill="primary" size="sm" label="Button sm" />
  <Button fill="primary" label="Button default" />
  <Button fill="primary" size="lg" label="Button lg" />
  <Button fill="primary" size="xl" label="Button xl" />
</div>

```vue
<Button fill="primary" size="xs" label="Button xs" />
<Button fill="primary" size="sm" label="Button sm" />
<Button fill="primary" label="Button default" />
<Button fill="primary" size="lg" label="Button lg" />
<Button fill="primary" size="xl" label="Button xl" />
```

<div>
  <Button fill="primary" icon="check" size="xs" label="Button xs" />
  <Button fill="primary" icon="check" size="sm" label="Button sm" />
  <Button fill="primary" icon="check" label="Button default" />
  <Button fill="primary" icon="check" size="lg" label="Button lg" />
  <Button fill="primary" icon="check" size="xl" label="Button xl" />
</div>

```vue
small" /> <Button colorfillprimary" icon="check" size="xs" label="Button xs" />
<Button fill="primary" icon="check" size="sm" label="Button sm" />
<Button fill="primary" icon="check" label="Button default" />
<Button fill="primary" icon="check" size="lg" label="Button lg" />
<Button fill="primary" icon="check" size="xl" label="Button xl" />
```

### Сравнение высоты кнопки с иконкой и без

<div>
  <Button size="xl" icon="check" label="Button xl" />
  <Button size="xl" label="Button xl" />
  <Button size="xl" label="Button xl caps" caps />
  <Button size="xl" icon="check" label="Button xl caps" caps />
</div>

<div>
  <Button size="lg" icon="check" label="Button lg" />
  <Button size="lg" label="Button xl" />
  <Button size="lg" label="Button xl caps" caps />
  <Button size="lg" icon="check" label="Button xl caps" caps />
</div>

<div>
  <Button icon="check" label="Button default" />
  <Button label="Button default" />
  <Button label="Button default caps" caps />
  <Button icon="check" label="Button default caps" caps />
</div>

<div>
  <Button size="sm" icon="check" label="Button sm" />
  <Button size="sm" label="Button sm" />
  <Button size="sm" label="Button sm caps" caps />
  <Button size="sm" icon="check" label="Button sm caps" caps />
</div>

<div>
  <Button size="xs" icon="check" label="Button xs" />
  <Button size="xs" label="Button xs" />
  <Button size="xs" label="Button xs caps" caps />
  <Button size="xs" icon="check" label="Button xs caps" caps />
</div>

### Широкая

<div>
  <Button block label="Button block" />
  <Button color="secondary" block label="Button block" />
  <Button fill="secondary" block label="Button block" />
  <Button fill="primary" size="xl" block label="Button block" />
</div>

```vue
<Button block label="Button block" />
<Button color="secondary" block label="Button block" />
<Button fill="secondary" block label="Button block" />
<Button fill="primary" size="xl" block label="Button block" />
```

## Группа кнопок

Комбинирует несколько кнопок в группу.

<div>
  <ButtonGroup>
    <Button>Первый</Button>
    <Button>Второй</Button>
    <Button>Третий</Button>
  </ButtonGroup>
</div>

```vue
<ButtonGroup>
  <Button>Первый</button>
  <Button>Второй</button>
  <Button>Третий</button>
</ButtonGroup>
```

### Цветные

<div>
  <ButtonGroup>
    <Button color="success">Первый</Button>
    <Button color="accent">Второй</Button>
    <Button color="primary">Третий</Button>
  </ButtonGroup>
</div>

```vue
<ButtonGroup>
  <Button color="success">Первый</button>
  <Button color="accent">Второй</button>
  <Button color="primary">Третий</button>
</ButtonGroup>
```

### Цветные с иконкой

<div>
  <ButtonGroup>
    <Button color="success" icon="check">Первый</Button>
    <Button color="accent" icon="remove">Второй</Button>
    <Button color="primary" icon="send">Третий</Button>
  </ButtonGroup>
</div>

```vue
<ButtonGroup>
  <Button color="success" icon="check">Первый</button>
  <Button color="accent" icon="remove">Второй</button>
  <Button color="primary" icon="send">Третий</button>
</ButtonGroup>
```

### С фоном

<div>
  <ButtonGroup>
    <Button fill="primary">Первый</Button>
    <Button fill="primary">Второй</Button>
    <Button fill="primary">Третий</Button>
  </ButtonGroup>
</div>

```vue
<ButtonGroup>
  <Button fill="primary">Первый</button>
  <Button fill="primary">Второй</button>
  <Button fill="primary">Третий</button>
</ButtonGroup>
```

### С фоном с иконкой

<div>
  <ButtonGroup>
    <Button fill="primary" icon="check">Первый</Button>
    <Button fill="primary" icon="remove">Второй</Button>
    <Button fill="primary" icon="send">Третий</Button>
  </ButtonGroup>
</div>

```vue
<ButtonGroup>
  <Button fill="primary" icon="check">Первый</button>
  <Button fill="primary" icon="remove">Второй</button>
  <Button fill="primary" icon="send">Третий</button>
</ButtonGroup>
```

## Анимация

<div>
<Button color="success" loading>Первый</Button>
<Button color="accent" loading>Второй</Button>
<Button color="primary" loading>Третий</Button>
</div>

```vue
<Button color="success" loading>Первый</Button>
<Button color="accent" loading>Второй</Button>
<Button color="primary" loading>Третий</Button>
```
