---
title: Checkbox
category: controls

checked: false
checkboxGroupModel: []
checkboxGroupLabels:
  [
    'Chuck Norris',
    'Bruce Lee',
    'Jackie Chan',
    'Jet Li',
    'Arnold Schwarzenegger',
  ]
checkboxLoopModel: []
checkboxLoopLabels: ['Angular', 'React', 'Vue']
checkboxValuesList: ['Github', 'Facebook', 'Twitter']
checkboxGroupSlotModel: []
groupModel: []
---

> TODO Запилить представление группы как Tabs, ButtonGroup

## Props

| Name  | Type           | Default   | Description                                    |
| ----- | -------------- | --------- | ---------------------------------------------- |
| value | String, Number | undefined | Устанавливает значение. Используйте `v-model`. |
| label | String         | undefined | Устанавливает метку.                           |

## Одиночный чекбокс

<box>
  <div>
    <Checkbox v-model="checked" label="Checkbox Label" />
    <Checkbox v-model="checked" label="Checkbox disabled" disabled />
  </div>
  <div>
    <code>checked: {{ checked }}</code>
  </div>
</box>

```
<Checkbox v-model="checked" label="Checkbox Label" />
```

## Одиночные чекбоксы в цикле

<box>
  <div>
    <Checkbox
      v-for="label in checkboxLoopLabels"
      :key="label"
      v-model="checkboxLoopModel"
      :label="label"
    />
  </div>
  <div><code>checkboxLoopModel: {{ checkboxLoopModel }}</code></div>
</box>

```
<Checkbox
  v-for="label in checkboxLoopLabels"
  :key="label"
  v-model="checkboxLoopModel"
  :label="label"
/>

<script>
  checkboxLoopModel: []
  checkboxLoopLabels: ['Angular', 'React', 'Vue']
</script>
```

## Группа чекбоксов

| Name    | Type   | Default        | Description                                                       |
| ------- | ------ | -------------- | ----------------------------------------------------------------- |
| value   | Array  | []             | Устанавливает значения. Используйте `v-model`.                    |
| labels  | Array  | []             | Устанавливает метки.                                              |
| present | String | 'button-group' | Меняет представление как группа кнопок. См. `Tabs`, `ButtonGroup` |

<box>
  <CheckboxGroup v-model="checkboxGroupModel" :labels="checkboxGroupLabels" />
  <div><code>checkboxGroupModel: {{ checkboxGroupModel }}</code></div>
</box>

```
<CheckboxGroup v-model="checkboxGroupModel" :labels="checkboxGroupLabels" />

<script>
  checkboxGroupModel: [],
  checkboxGroupLabels: [
    'Chuck Norris',
    'Bruce Lee',
    'Jackie Chan',
    'Jet Li',
    'Arnold Schwarzenegger'
  ]
</script>
```

## Оформление как группы кнопок

Добавьте свойство `present="button-group"`

<box>
  <CheckboxGroup v-model="checkboxGroupModel" :labels="checkboxGroupLabels" present="button-group" />
  <div><code>checkboxGroupModel: {{ checkboxGroupModel }}</code></div>
</box>

```
<CheckboxGroup v-model="checkboxGroupModel" :labels="checkboxGroupLabels" present="button-group" />
```

## Группа чекбоксов в слоте

Используйте CheckboxGroup в сочетании с массивом, чтобы создать композицию. В этом случае Checkbox использует метку для состояния `v-model`. Значение каждой метки можно настроить. Если не заполнено, по умолчанию будет использоваться значение свойства label.

<box>
  <CheckboxGroup v-model="checkboxGroupSlotModel">
    <Checkbox label="Angular" />
    <Checkbox label="React" />
    <Checkbox label="Vue" />
  </CheckboxGroup>
  <div><code>checkboxGroupSlotModel: {{ checkboxGroupSlotModel }}</code></div>
</box>

```
<CheckboxGroup v-model="checkboxGroupSlotModel">
  <Checkbox label="Angular" value="Angular" />
  <Checkbox label="React" value="React" />
  <Checkbox label="Vue" value="Vue" />
</CheckboxGroup>
```

## HTML в слоте

<box>
  <CheckboxGroup v-model="groupModel">
    <Checkbox label="account">
      <Icon name="account" />
    </Checkbox>
    <Checkbox label="assignment">
      <Icon name="assignment" />
    </Checkbox>
    <Checkbox label="send">
      <Icon name="send" />
    </Checkbox>
  </CheckboxGroup>
  <div><code>groupModel: {{ groupModel }}</code></div>
</box>

```
<CheckboxGroup v-model="groupModel">
  <Checkbox label="account">
    <Icon name="account" />
  </Checkbox>
  <Checkbox label="assignment">
    <Icon name="assignment" />
  </Checkbox>
  <Checkbox label="send">
    <Icon name="send" />
  </Checkbox>
</CheckboxGroup>
```
