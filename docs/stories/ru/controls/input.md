---
title: Input
category: controls
---

## Props

| Name          | Type           | Default  | Description                                                                      |
| ------------- | -------------- | -------- | -------------------------------------------------------------------------------- |
| value         | String, Number | ''       | Устанавливает значение. Для двухсторонней привязки данных используйте `v-model`. |
| label         | String         | ''       | Устанавливает метку.                                                             |
| icon          | String         | null     | Устанавливает иконку. Смотри [icons](/components/icons).                         |
| icon-position | String         | 'inside' | Устанавливает расположение иконки снаружи `inside` или внутри `outside`.         |
| clearable     | Boolean        | false    | Отображает кнопку очистки поля.                                                  |
| debounce      | Number         | 0        | Устанавливает задержку выполнения. Полезно для поисковых запросов в API.         |

Также можно передавать [обычные атрибуты](https://developer.mozilla.org/ru/docs/Web/HTML/Element/Input): `type, id, autofocus, required, placeholder...`

## Slots

```
<slot name="prepend" />
<slot name="append" />
```

Передает контент в `$slots.prepend` (перед инпутом) и в `$slots.append` (после инпута).

## Примеры

<Input
  id="username"
  icon="search"
  type="email"
  placeholder="user@ozon.ru"
  v-model="$parent.$parent.username"
  required
  autofocus
  clearable
/>

```
<Input
  id="username"
  icon="search"
  type="email"
  placeholder="user@ozon.ru"
  v-model="username"
  required
  autofocus
  clearable
/>
```

### Иконка снуружи

<Input
  icon="account"
  icon-position="outside"
  type="password"
  value="password"
  required
/>

```
<Input
  icon="account"
  icon-position="outside"
  type="password"
  value="password"
  required
/>
```

## Пример со слотами

> TODO Зарефачить враппер `.root`, стобы растопыривало

<Input>
  <div slot="prepend" class="slot">
    <Button label="Удалить" />
  </div>
  <div slot="append" class="slot">
    <b style="border: 1px solid teal; padding: .5rem">Wow!</b>
  </div>
</Input>

```
<Input>
  <div slot="prepend" class="slot">prepend</div>
  <div slot="append" class="slot">append</div>
</Input>
```

## Многострочное текстовое поле

<Input
  id="textarea"
  type="textarea"
  rows="5"
  placeholder="Многострочное текстовое поле..."
/>

```
<Input
  id="textarea"
  type="textarea"
  rows="5"
  placeholder="Многострочное текстовое поле..."
/>
```
