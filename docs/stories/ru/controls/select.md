---
title: Select

datalist:
  - {
      value: '89aeb593-1551-4b3a-b99c-41baba1ad77c',
      label: 'Коммерческий департамент',
      children: [],
    }
  - {
      value: '9f4666bf-8f29-4bd7-a8db-5bc103cb6c02',
      label: 'Маркетинг Реклама',
      children: [],
    }
  - {
      value: 'aabb6f11-5d9e-47da-a541-2d37503789ee',
      label: 'Маркетплейс',
      disabled: true,
      children: [],
    }
  - {
      value: 'd6741011-778d-4305-b807-6b814e0f17b8',
      label: 'Озон Инвест',
      children: [],
    }
  - {
      value: '0dca913f-eb5d-4639-980a-ce58afcae9be',
      label: 'Отдел складской логистики',
      children: [],
    }

modelDeep:
  [
    '442abd71-26fd-41c1-820d-1554504fe221',
    '89aeb593-1551-4b3a-b99c-41baba1ad77c',
    '9f4666bf-8f29-4bd7-a8db-5bc103cb6c02',
  ]

model:
  [
    '442abd71-26fd-41c1-820d-1554504fe221',
    '89aeb593-1551-4b3a-b99c-41baba1ad77c',
    '9f4666bf-8f29-4bd7-a8db-5bc103cb6c02',
  ]
modelString: '89aeb593-1551-4b3a-b99c-41baba1ad77c'
modelArray:
  [
    '442abd71-26fd-41c1-820d-1554504fe221',
    '89aeb593-1551-4b3a-b99c-41baba1ad77c',
    '9f4666bf-8f29-4bd7-a8db-5bc103cb6c02',
  ]
jsonDeep:
  - {
      value: '00000000-0000-0000-0000-000000000000',
      label: 'Головное подразделение',
      children:
        [
          {
            value: '442abd71-26fd-41c1-820d-1554504fe221',
            label: 'Банк',
            parentId: '00000000-0000-0000-0000-000000000000',
            children: [],
          },
          {
            value: '650eb4a0-73fd-44d2-8dd0-411be3ed5ebd',
            label: 'Бухгалтерия',
            parentId: '00000000-0000-0000-0000-000000000000',
            disabled: true,
            children: [],
          },
          {
            value: '13a555ed-aa9c-4eaa-ae36-33ef16f91f13',
            label: 'ВЭД',
            parentId: '00000000-0000-0000-0000-000000000000',
            disabled: true,
            children: [],
          },
          {
            value: '89aeb593-1551-4b3a-b99c-41baba1ad77c',
            label: 'Коммерческий департамент',
            parentId: '00000000-0000-0000-0000-000000000000',
            children: [],
          },
          {
            value: '9f4666bf-8f29-4bd7-a8db-5bc103cb6c02',
            label: 'Маркетинг Реклама',
            parentId: '00000000-0000-0000-0000-000000000000',
            children: [],
          },
          {
            value: 'aabb6f11-5d9e-47da-a541-2d37503789ee',
            label: 'Маркетплейс',
            parentId: '00000000-0000-0000-0000-000000000000',
            disabled: true,
            children: [],
          },
          {
            value: 'd6741011-778d-4305-b807-6b814e0f17b8',
            label: 'Озон Инвест',
            parentId: '00000000-0000-0000-0000-000000000000',
            children: [],
          },
          {
            value: '0dca913f-eb5d-4639-980a-ce58afcae9be',
            label: 'Отдел складской логистики',
            parentId: '00000000-0000-0000-0000-000000000000',
            children: [],
          },
          {
            value: 'cc7bc92c-4b6f-4444-ae22-e06e54807eb2',
            label: 'Расчетный отдел',
            parentId: '00000000-0000-0000-0000-000000000000',
            children: [],
          },
          {
            value: 'db0690f9-cbea-451f-b74d-3866be79bf8c',
            label: 'Сертификаты',
            parentId: '00000000-0000-0000-0000-000000000000',
            children: [],
          },
          {
            value: '64c9df88-91c1-45dc-ac49-0be007b7a317',
            label: 'Факторинг',
            parentId: '00000000-0000-0000-0000-000000000000',
            disabled: true,
            children: [],
          },
          {
            value: '9c7bcd84-7a08-44aa-b9c3-9fc77964b866',
            label: 'Центр Обслуживания коммерческой функции',
            parentId: '00000000-0000-0000-0000-000000000000',
            children:
              [
                {
                  value: '84793fa0-4065-4d87-be7d-5a66d9a4bcd2',
                  label: 'Группа сверок с контрагентами',
                  parentId: '9c7bcd84-7a08-44aa-b9c3-9fc77964b866',
                  disabled: true,
                  children: [],
                },
                {
                  value: '4264688d-7468-4151-85c7-2051b67cceea',
                  label: 'Центр обслуживания коммерческой функции по договорному сопровождению',
                  parentId: '9c7bcd84-7a08-44aa-b9c3-9fc77964b866',
                  children: [],
                },
                {
                  value: 'a6b36057-838d-4de1-a331-41d7c77c5920',
                  label: 'Центр обслуживания маркетинга и мерчантов',
                  parentId: '9c7bcd84-7a08-44aa-b9c3-9fc77964b866',
                  children: [],
                },
              ],
          },
          {
            value: 'fc2ded34-7517-449a-a823-fbce0464588c',
            label: 'Черновики',
            parentId: '00000000-0000-0000-0000-000000000000',
            disabled: true,
            children:
              [
                {
                  value: '0fedb49c-de86-4dd1-9b6f-ba4d1f55b14d',
                  label: 'Ошибки',
                  parentId: 'fc2ded34-7517-449a-a823-fbce0464588c',
                  children: [],
                },
                {
                  value: '035bc15e-1d45-45db-a406-3fb8e35347d5',
                  label: 'Тест',
                  parentId: 'fc2ded34-7517-449a-a823-fbce0464588c',
                  disabled: true,
                  children: [],
                },
              ],
          },
          {
            value: '0614a6bf-bf70-443f-9bf1-ab208d4ff6d8',
            label: 'Юридический департамент',
            parentId: '00000000-0000-0000-0000-000000000000',
            children: [],
          },
        ],
    }
  - {
      value: '99999999-9999-9999-9999-999999999999',
      label: 'Другое',
      children: [],
    }
---

## Props

| Name        | Type    | Default       | Description                              |
| ----------- | ------- | ------------- | ---------------------------------------- |
| value       | Array   | []            | v-model                                  |
| datalist    | Array   | []            | Массив объектов с данными                |
| placeholder | String  | 'Выберите...' |                                          |
| skipParent  | Boolean | false         | Не добавлять в модель значения родителей |

**JSON**

```
{{ jsonDeep }}
```

## Вложенные данные

### Выбор родителя и потомков

<!-- <pre>{{ modelDeep }}</pre> -->
<SelectDeep v-model="modelDeep" :datalist="jsonDeep" />

```
<SelectDeep v-model="modelDeep" :datalist="datalist" />
```

### Выбор только потомков

<!-- <pre>{{ model }}</pre> -->
<SelectDeep v-model="model" :datalist="jsonDeep" skip-parent />

```
<SelectDeep v-model="model" :datalist="datalist" skip-parent />
```

## Плоские данные

### Одиночный выбор

<!-- <pre>{{ modelString }}</pre> -->
<Select v-model="modelString" :datalist="datalist" />

```
<Select v-model="modelString" :datalist="datalist" />
```

### Множественный выбор

<!-- <pre>{{ modelArray }}</pre> -->
<Select v-model="modelArray" :datalist="datalist" multiple />

```
<Select v-model="modelArray" :datalist="datalist" multiple />
```
