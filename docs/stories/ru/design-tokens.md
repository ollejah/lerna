---
title: Design Tokens
order: 4
---

**code is poetry**

Is a collection of reusable components and tools, guided by clear standards, that can be assembled together to build digital products
