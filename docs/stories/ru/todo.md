---
title: Todo
---

## Песочница

- Подсветка TOC при скролле IntersectionObserver API
- Причесать токенайзеры для `marked.Renderer` согласно API
- Поиск по страницам
- Версионирование песочницы
- Было бы неплохо раскурить тесты...

## Библиотека

- Добавить в библиотеку уже готовые и используемые в приложениях компоненты, модули и утилиты (поэтапно)
- Проверить каждый добавленный компонент на корректный билд и интеграцию в приложение-песочницу
- Провести рефакторинг, если необходимо
- Добавить стори с описанием и примерами для каждого компонента
- По окончании успешных работ публиковать библиотеку с версионированием

> Настроить версионирование при публикации и создание `changelog`

## Перенести

- DataGrid (примеры, доки, песочница)
- Observer
- FileUpload
- TagList
- SpinLoader

* App Layouts (примеры, доки, песочница)

  - AppUpdatePrompt
  - AsideMenu
  - Footer
  - Logo
  - Navigation
  - Notices
  - Offline
  - UserMenu

* Компоненты, специфичные для приложений (примеры, доки, песочница)
  - Page
  - PageList
  - PageDetails
  - PageDetailsList
  - PageRow
  - PageSection
  - PageSectionTitle
  - Pagination
  - PaginationMeta
  - PaginationSimple
  - Report
  - Barcode
  - AnimationThreeDots
  - DetailsCard
  - NotFound
  - DocumentPreview
  - MonacoEditor

## Планы

- Плагины (примеры, доки, песочница)
- Autocomplete (примеры, доки, песочница)
- WIP: Календарь (примеры, доки, песочница)

## Дизайн-система 

- примеры, доки
