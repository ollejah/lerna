---
title: Components
category: components
order: 2
---

## Style Guides

[Vue.js](https://vuejs.org/v2/style-guide/)
[Nuxt.js](https://nuxtjs.org/guide)

**Strongly Recommended (Improving Readability)**

* [Single-file component filename casing](https://vuejs.org/v2/style-guide/#Single-file-component-filename-casing-strongly-recommended)
* [Base component names](https://vuejs.org/v2/style-guide/#Base-component-names-strongly-recommended)
* [Single-instance component names](https://vuejs.org/v2/style-guide/#Single-instance-component-names-strongly-recommended)
* [Order of words in component names](https://vuejs.org/v2/style-guide/#Order-of-words-in-component-names-strongly-recommended)

## Функциональные компоненты

> Если ваш компонент не нуждается в отслеживании состояния, превращение его в функциональные компоненты может повысить производительность рендеринга на 70%

Функциональные компоненты (не путать с функциями рендеринга Vue) — это компонент, который не содержит ни состояния, ни экземпляра.

Проще говоря это означает, что компонент не поддерживает реактивность и не может сделать ссылку на себя через ключевое слово this.

**Доступ к свойствам компонента**

Если у компонента нет состояния или экземпляра вы можете задаться вопросом, как можно ссылаться на такие вещи, как данные или методы. К счастью, Vue предоставляет параметр «context» для базовой функции рендеринга.

**`context` является объектом со следующими свойствами:**

* `props`: Объект предоставляющий свойства props
* `children`: Массив дочерних элементов VNode
* `slots`: Функция, возвращающая объект slots
* `scopedSlots`: (v2.6.0+) Объект, который предоставляет переданные scoped слоты. Также устанавливает нормальные слоты как функции.
* `data`: Весь объект данных, переданный компоненту как второй аргумент createElement
* `parent`: Ссылка на родительский компонент
* `listeners`: (v2.3.0+) Объект, содержащий зарегистрированных родителей event listeners. Это псевдоним data.on
* `injections`: (v2.3.0+) При использовании параметра inject он будет содержать разрешенные инъекции.

**Зачем использовать функциональные компоненты?**

Скорость.

Поскольку функциональные компоненты не имеют состояния, они не требуют дополнительной инициализации для таких вещей, как система реактивности Vue.

Функциональные компоненты будут по-прежнему реагировать на изменения входных данных, такие как новые props, но внутри самого компонента нет способа узнать, когда его данные изменились, поскольку он не поддерживает свое собственное состояние.

Есть несколько отличных вариантов использования функциональных компонентов:

* Простые компоненты, без внутренней логики. Например, кнопки, теги, карточки, даже полные страницы со статичным текстом, типа страница «About».
* «Компоненты более высокого порядка», которые используется для создания разметки или основных функций вокруг другого компонента.
* Каждый раз, когда вы попадаете в цикл (v-for), элементы цикла обычно так же являются подходящими кандидатами.

## Полезные ссылки

* [Lazy Resolving Observable API Services with Vue.js](https://markus.oberlehner.net/blog/lazy-resolving-observable-api-services-with-vue/)
* [Avoid Opaque Dependency Injection Techniques with Vue.js](https://markus.oberlehner.net/blog/avoid-opaque-dependency-injection-techniques-with-vuejs/)
* [Application State Management with Vue 3](https://markus.oberlehner.net/blog/application-state-management-with-vue-3/)
* [The IoC Container Pattern with Vue.js](https://markus.oberlehner.net/blog/the-ioc-container-pattern-with-vue)