---
title: SSO
category: modules
---

## Установка

> Убедитесь в наличии файла `.npmrc` с содержимым `@ozon-erp:registry=https://nexus.s.o3.ru/repository/npm-private/`

```
npm install @ozon-erp/sso
