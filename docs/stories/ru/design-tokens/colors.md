---
title: Colors
colors:
  - 'primary'
  - 'secondary'
  - 'accent'
  - 'success'
  - 'warning'
  - 'error'
  - 'dark-10'
  - 'dark-20'
  - 'dark-30'
  - 'dark-40'
  - 'dark-50'
  - 'dark-60'
  - 'dark-70'
  - 'dark-80'
  - 'dark-90'
  - 'grey'
  - 'dark'
---

## Базовые цвета

Базовые цвета определяются в конфиге библиотеки и доступны как [CSS custom properties](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties)

```css
:root {
  --primary: $primary;
  --secondary: $secondary;
  --accent: $accent;
  --success: $success;
  --warning: $warning;
  --error: $error;
  --dark: $dark;
  --grey: $grey;
}
```

## Text Color

Утилиты для управления цветом текста элемента.<br>
`<div class="text-primary" />`

<table>
  <thead>
    <tr>
      <th>Class</th>
      <th>Properties</th>
      <th>Preview</th>
    </tr>
  </thead>
  <tbody>
    <tr v-for="(item, idx) in colors" :key="idx">
      <td>.text-{{ item }}</td>
      <td>color: ${{ item }};</td>
      <td><span :class="`text-${item}`">Aa Zz</span></td>
    </tr>
  </tbody>
</table>

## Background Color

Утилиты для управления цветом фона элемента.<br>
`<div class="bg-primary" />`

<table>
  <thead>
    <tr>
      <th>Class</th>
      <th>Properties</th>
      <th>Preview</th>
    </tr>
  </thead>
  <tbody>
    <tr v-for="(item, idx) in colors" :key="idx">
      <td>.bg-color-{{ item }}</td>
      <td>background-color: ${{ item }};</td>
      <td><div :class="`bg-${item}`">&nbsp;</div></td>
    </tr>
  </tbody>
</table>
