---
title: Spaces
spacesX:
  - { 'space-x-1': '0.25rem' }
  - { 'space-x-2': '0.5rem' }
  - { 'space-x-3': '0.75rem' }
  - { 'space-x-4': '1rem' }
  - { 'space-x-5': '1.25rem' }
  - { 'space-x-6': '1.5rem' }
  - { 'space-x-8': '2rem' }
  - { 'space-x-10': '2.5rem' }
  - { 'space-x-12': '3rem' }

spacesY:
  - { 'space-y-1': '0.25rem' }
  - { 'space-y-2': '0.5rem' }
  - { 'space-y-3': '0.75rem' }
  - { 'space-y-4': '1rem' }
  - { 'space-y-5': '1.25rem' }
  - { 'space-y-6': '1.5rem' }
  - { 'space-y-8': '2rem' }
  - { 'space-y-10': '2.5rem' }
  - { 'space-y-12': '3rem' }
---

Утилиты для управления пространством между дочерними элементами.

## Space-X

Управляйте горизонтальным расстоянием между элементами, используя <var>space-x-{amount}</var> утилиты.

`<style>table { border: 10px solid red; }</style>`

<table>
  <thead>
    <tr>
      <th>Class</th>
      <th>Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr v-for="(item, idx) in spacesX" :key="idx">
      <td>
        <span>.{{ Object.keys(item).toString() }}</span>
        <span class="-extra">> * + *</span>
      </td>
      <td>margin-left: {{ Object.values(item).toString() }};</td>
    </tr>
  </tbody>
</table>

### space-x-2

<section class="flex space-x-2">
  <div v-for="i in 5" :key="i" class="design-item">{{ i }}</div>
</section>

### space-x-4

<section class="flex space-x-4">
  <div v-for="i in 5" :key="i" class="design-item">{{ i }}</div>
</section>

### space-x-8

<section class="flex space-x-8">
  <div v-for="i in 5" :key="i" class="design-item">{{ i }}</div>
</section>

## Space-Y

Управляйте вертикальным расстоянием между элементами, используя <var>space-y-{amount}</var> утилиты.

<table>
  <thead>
    <tr>
      <th>Class</th>
      <th>Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr v-for="(item, idx) in spacesY" :key="idx">
      <td>
        <span>.{{ Object.keys(item).toString() }}</span>
        <span class="-extra">> * + *</span>
      </td>
      <td>margin-top: {{ Object.values(item).toString() }};</td>
    </tr>
  </tbody>
</table>

### space-y-2

<section class="flex-col space-y-2">
  <div v-for="i in 5" :key="i" class="design-item">{{ i }}</div>
</section>

### space-y-4

<section class="flex-col space-y-4">
  <div v-for="i in 5" :key="i" class="design-item">{{ i }}</div>
</section>

### space-y-8

<section class="flex-col space-y-8">
  <div v-for="i in 5" :key="i" class="design-item">{{ i }}</div>
</section>
