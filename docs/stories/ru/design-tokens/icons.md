---
title: Icons
---

<section>

В приложении собирается `svg-sprite` на основе иконок, располженных в `~/icons`. В файле `~/icons.index.js` подключаются все иконки через `require.context`, на выходе экспортируется список доступных иконок.

## Props

| Props | Type   | Default   | Description                                                                           |
| ----- | ------ | --------- | ------------------------------------------------------------------------------------- |
| name  | String | undefined | Название иконки.                                                                      |
| size  | String | 20px      | Размеры. Доступны: `xs: 14px, sm: 16px, default: 20px, lg: 24px, xl: 32px, xxl: 48px` |
| color | String | undefined | Устанавливает цвет                                                                    |

<box>
  <div flex="row center-spread">
  <Icon name="check" size="xs" color="primary" />
  <Icon name="check" size="sm" color="primary" />
  <Icon name="check" color="primary" />
  <Icon name="check" size="lg" color="primary" />
  <Icon name="check" size="xl" color="primary" />
  <Icon name="check" size="xxl" color="primary" />
  </div>
</box>

```vue live
<Icon name="check" color="primary" />
```

**TODO:**

- Актуальныйкомпонет не меняет иконку динамически, заточен под билд спрайта...
- Возможно, стоит сделать передачу кастомных размеров в пикселах?

</section>

## Базовый набор

<StoryIcons />
