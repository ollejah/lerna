---
title: Badge
category: components
---

Используется для установки дополнительного акцента, например, статус, категория, версия какой-либо сущности. По умолчанию установлено ограничение на ширину тега в `10rem`, ограничение можно снять свойством `no-truncate`.

## Props

| Name        | Type    | Default   | Description                                                           |
| ----------- | ------- | --------- | --------------------------------------------------------------------- |
| label       | String  | undefined | Устанавливает текст кнопки. Также можно просто передать слот.         |
| color       | String  | undefined | Устанавливает цвет текста. [пример](#базовые-цвета)                   |
| fill        | String  | undefined | Устанавливает цвет фона и инвертирует цвет текста. [пример](#с-фоном) |
| size        | String  | undefined | Устанавливает размер. [пример](#размеры)                              |
| ghost       | Boolean | false     | Устанавливает прозрачный фон [пример](#прозрачные)                    |
| circle      | Boolean | false     | Делает кнопку с закругленными углами [пример](#закругленные)          |
| icon        | String  | undefined | Устанавливает иконку. [пример](/components/icons)                     |
| no-truncate | Boolean | false     | Снимает ограничение ширины [пример](#анимация)                        |
| hint        | String  | undefined | Показывает тултип                                                     |

**Пример**

`<Badge>Badge Label</Badge>` или `<Badge label="Badge Label" />`

```vue live
<Badge
  label="Отправлен провайдеру"
  color="success"
  icon="check"
  hint="Документ отправлен провайдеру"
/>
```

## Базовые цвета

<div>
<Badge label="Документ 416" />
<Badge label="Документ ЭДО" color="primary" />
<Badge label="Поставка Metazon" color="info" />
<Badge label="Принят с ошибками" color="error" />
<Badge label="Отправлен провайдеру" color="success" />
<Badge label="Ожидает подтверждения" color="warning" />
</div>

```vue
<Badge label="Документ 416" />
<Badge label="Документ ЭДО" color="primary" />
<Badge label="Поставка Metazon" color="info" />
<Badge label="Принят с ошибками" color="error" />
<Badge label="Отправлен провайдеру" color="success" />
<Badge label="Ожидает подтверждения" color="warning" />
```

## С фоном

<div>
<Badge label="Документ 416" fill="dark" />
<Badge label="Документ ЭДО" fill="primary" />
<Badge label="Поставка Metazon" fill="info" />
<Badge label="Принят с ошибками" fill="error" />
<Badge label="Отправлен провайдеру" fill="success" />
<Badge label="Ожидает подтверждения" fill="warning" />
</div>

```vue
<Badge label="Документ 416" fill="dark" />
<Badge label="Документ ЭДО" fill="primary" />
<Badge label="Поставка Metazon" fill="info" />
<Badge label="Принят с ошибками" fill="error" />
<Badge label="Отправлен провайдеру" fill="success" />
<Badge label="Ожидает подтверждения" fill="warning" />
```

## Прозрачные

### На светлом фоне

<div>
<Badge label="Документ 416" ghost />
<Badge label="Документ ЭДО" color="primary" ghost />
<Badge label="Поставка Metazon" color="info" ghost />
<Badge label="Принят с ошибками" color="error" ghost />
<Badge label="Отправлен провайдеру" color="success" ghost />
<Badge label="Ожидает подтверждения" color="warning" ghost />
</div>

```vue
<Badge label="Документ 416" ghost />
<Badge label="Документ ЭДО" color="primary" ghost />
<Badge label="Поставка Metazon" color="info" ghost />
<Badge label="Принят с ошибками" color="error" ghost />
<Badge label="Отправлен провайдеру" color="success" ghost />
<Badge label="Ожидает подтверждения" color="warning" ghost />
```

### На темном фоне

Добавьте свойство `dark-mode` кнопке или переключитесь в темный режим интерфейса

<div style="background-color: var(--dark); padding: 1rem">
<Badge label="Документ 416" ghost dark-mode />
<Badge label="Документ ЭДО" color="primary" ghost dark-mode />
<Badge label="Поставка Metazon" color="info" ghost dark-mode />
<Badge label="Принят с ошибками" color="error" ghost dark-mode />
<Badge label="Отправлен провайдеру" color="success" ghost dark-mode />
<Badge label="Ожидает подтверждения" color="warning" ghost dark-mode />
</div>

<div style="background-color: var(--dark); padding: 1rem">
<Badge icon="check" label="Документ 416" ghost dark-mode />
<Badge icon="check" label="Документ ЭДО" color="primary" ghost dark-mode />
<Badge icon="check" label="Поставка Metazon" color="info" ghost dark-mode />
<Badge icon="check" label="Принят с ошибками" color="error" ghost dark-mode />
<Badge icon="check" label="Отправлен провайдеру" color="success" ghost dark-mode />
<Badge icon="check" label="Ожидает подтверждения" color="warning" ghost dark-mode />
</div>

```vue
<Badge label="Документ 416" ghost dark-mode />
<Badge label="Документ ЭДО" color="primary" ghost dark-mode />
<Badge label="Поставка Metazon" color="info" ghost dark-mode />
<Badge label="Принят с ошибками" color="error" ghost dark-mode />
<Badge label="Отправлен провайдеру" color="success" ghost dark-mode />
<Badge label="Ожидает подтверждения" color="warning" ghost dark-mode />
```

## С иконокой

<div>
<Badge icon="check" label="Документ 416" color="dark" />
<Badge icon="check" label="Документ ЭДО" color="primary" />
<Badge icon="check" label="Поставка Metazon" color="info" />
<Badge icon="check" label="Принят с ошибками" color="error" />
<Badge icon="check" label="Отправлен провайдеру" color="success" />
<Badge icon="check" label="Ожидает подтверждения" color="warning" />
</div>

<div>
<Badge icon="check" label="Документ 416" fill="dark" />
<Badge icon="check" label="Документ ЭДО" fill="primary" />
<Badge icon="check" label="Поставка Metazon" fill="info" />
<Badge icon="check" label="Принят с ошибками" fill="error" />
<Badge icon="check" label="Отправлен провайдеру" fill="success" />
<Badge icon="check" label="Ожидает подтверждения" fill="warning" />
</div>

```vue
<Badge icon="check" label="Документ 416" fill="dark" />
<Badge icon="check" label="Документ ЭДО" fill="primary" />
<Badge icon="check" label="Поставка Metazon" fill="info" />
<Badge icon="check" label="Принят с ошибками" fill="error" />
<Badge icon="check" label="Отправлен провайдеру" fill="success" />
<Badge icon="check" label="Ожидает подтверждения" fill="warning" />
```

## Закругленные

<div>
<Badge label="Документ 416" fill="dark" circle />
<Badge label="Документ ЭДО" fill="primary" circle />
<Badge label="Поставка Metazon" fill="info" circle />
<Badge label="Принят с ошибками" fill="error" circle />
<Badge label="Отправлен провайдеру" fill="success" circle />
<Badge label="Ожидает подтверждения" fill="warning" circle />
</div>

```vue
<Badge label="Документ 416" fill="dark" circle />
<Badge label="Документ ЭДО" fill="primary" circle />
<Badge label="Поставка Metazon" fill="info" circle />
<Badge label="Принят с ошибками" fill="error" circle />
<Badge label="Отправлен провайдеру" fill="success" circle />
<Badge label="Ожидает подтверждения" fill="warning" circle />
```

## Размеры

> Доступны `xl, lg, sm, xs`. Размеры устанавливаются относительно `<body>`, не в `rem`, а в `em`. В нашем случаем размер текста у `<body>` равен 14px, от него и рассчитываем. Избегайте устанавливать размер шрифта родительскому контейнеру, в который помещается копмонент, в этом случае его размер будет рассчитываться от размера шрифта контейнера.

<div>
<Badge label="Утвержден" color="primary" size="xs" /> 
<Badge label="Утвержден" fill="primary" size="xs" /> 
<Badge icon="check" label="Утвержден" fill="primary" size="xs" />
</div>

<div>
<Badge label="Утвержден" color="primary" size="sm" /> 
<Badge label="Утвержден" fill="primary" size="sm" /> 
<Badge icon="check" label="Утвержден" fill="primary" size="sm" />
</div>

<div>
<Badge label="Утвержден" color="primary" /> 
<Badge label="Утвержден" fill="primary" /> 
<Badge icon="check" label="Утвержден" fill="primary" />
</div>

<div class="flex">
<Badge label="Утвержден" color="primary" size="lg" /> 
<Badge label="Утвержден" fill="primary" size="lg" /> 
<Badge icon="check" label="Утвержден" fill="primary" size="lg" />
</div>

<div>
<Badge label="Утвержден" color="primary" size="xl" /> 
<Badge label="Утвержден" fill="primary" size="xl" /> 
<Badge icon="check" label="Утвержден" fill="primary" size="xl" />
<p>Избегайте устанавливать размер шрифта <Badge label="Утвержден" color="primary" /> </p>
</div>
