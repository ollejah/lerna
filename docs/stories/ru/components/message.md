---
title: Message
category: components
---

**Всплывающее предупреждение (Notify, Toast)**

Компонент регистрируется как плагин и доступен глобально всему приложению через `this.$message`. Монтируется динамически.

## Установка

```js
import { Message } from '@ozon-erp/ui-lib/plugins'

// Задержка до исчезновения сообщения, сортировка потока сообщений
Vue.use(Message, { delay: 5000, order: 'desc' })

// Подружить с контекстом Nuxt
export default (ctx, inject) => {
  inject('message', Message)
}
```

## Props

| Name    | Type   | Default           | Description                        |
| ------- | ------ | ----------------- | ---------------------------------- |
| delay   | Number | 3000              | Задержка до исчезновения сообщения |
| content | String | asc (новые снизу) | Сортировка потока сообщений        |

## Methods

```js
// Сообщение в голубой тональности
this.$message.info('Текст сообщения')

// Сообщение в зеленой тональности
this.$message.success('Текст сообщения')

// Сообщение в желтой тональности
this.$message.warning('Текст сообщения')

// Сообщение в красной тональности
this.$message.error('Текст сообщения')
```

## Примеры

<Button v-on:click="$message.info('Операция завершена')" label="Показать info" color="info" />

<Button v-on:click="$message.success('Документ успешно отправлен')" label="Показать success" color="success" />

<Button v-on:click="$message.warning('Внимание, документ будет отправлен в ЦРПТ')" label="Показать warning" color="warning" />

<Button v-on:click="$message.error('Что-то пошло не так')" label="Показать error" color="error" />
