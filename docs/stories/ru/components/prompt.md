---
title: Prompt
category: components
---

**Диалог подтверждения**

> Имеет ли смысл переименовать в Confirm?

Компонент регистрируется как плагин и доступен глобально всему приложению через `this.$prompt`. Монтируется динамически.

## Установка

```js
import { Prompt } from '@ozon-erp/ui-lib/plugins'
Vue.use(Prompt)

// Подружить с контекстом Nuxt
export default (ctx, inject) => {
  inject('prompt', Prompt)
}
```

## Props

| Name        | Type     | Default                    | Description                      |
| ----------- | -------- | -------------------------- | -------------------------------- |
| title       | String   | 'Необходимо подтверждение' | Заголовок                        |
| content     | String   | undefined                  | Текст сообщения                  |
| acceptText  | String   | 'Да'                       | Кнопка подтверждения             |
| accept      | Function | return false               | Коллбэк для кнопки подтверждения |
| declineText | String   | 'Нет'                      | Кнопка отклонения                |
| decline     | Function | return false               | Коллбэк для кнопки отклонения    |

## Methods

```js
this.$prompt.show({
  accept: Function,
  decline: Function
})
```

## Примеры

> В примере используется [плагин сообщения](/stories/ru/components/prompt)

```js
methods: {
  this.$prompt.show({
    content: 'Документ будет отправлен в ЦРПТ. Вы уверены?',
    accept: async () => {
      try {
        content = JSON.stringify(JSON.parse(content))
        await this.$model.create({ content })
        this.$message.success('Документ отправлен')
      } catch {
        this.$message.error('Что-то пошло не так')
      }
    },
    decline: () => {
      this.$message.warning('Отменено')
    }
  })
}
```

<Button v-on:click="$prompt.show({title: 'Внимание', content: 'Документ будет отправлен в ЦРПТ. Вы уверены?', accept: () => $message.success('Документ отправлен'), decline: () => $message.error('Что-то пошло не так')})">Показать диалог</Button>
