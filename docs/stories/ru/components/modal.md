---
title: Modal
category: components

json:
  {
    id: 'eaaaf828-e714-4409-ab11-7d51e6086a6a',
    sgtin: '0246071352201034150611303712010A2537B',
    status: 'Ожидает подтверждения получения ,собственником',
    contractor:
      {
        name: 'Поставщик Двора Его Императорского Величества, АО',
        inn: '3811029172',
      },
    prod_name: 'Streptocidum',
    sell_name: 'Стрептоцид',
    endDelivery: '0001-01-01T00:00:00',
    sscc: '01946012345678963101000712',
  }
---

Компонент регистрируется как плагин и доступен глобально всему приложению через `this.$modal`. Может монтироваться динамически или статически, оборачивая необходимую разметку. Модальное окно можно закрыть по клавише `Esc` и по клику на оверлей.

## Установка

```js
import { Modal } from '@ozon-erp/ui-lib/plugins'
Vue.use(Modal)

// Подружить с контекстом Nuxt
export default (ctx, inject) => {
  inject('modal', Modal)
}
```

## Props

| Name                  | Type    | Default            | Description                                   |
| --------------------- | ------- | ------------------ | --------------------------------------------- |
| value                 | Boolean | false              | Условие, по которому окно открывается само :) |
| name                  | String  | 'root'             | Имя компонента для вызова                     |
| width                 | String  | '60rem'            | Ширина окна                                   |
| height                | String  | 'auto'             | Высота окна                                   |
| title                 | String  | 'Детали документа' | Заголовок окна                                |
| type                  | String  | 'html'             | Тип окна                                      |
| theme                 | String  | undefined          | Светлая или темная тема                       |
| disable-overlay-close | Boolean | false              | Запрет закрытия окна по клику на оверлей      |

## Slots

По умолчанию [обернутый контент](#wrap) рендерится в дефотный слот. Слот `footer` по умолчанию пустой.

```
<slot name="header" />
<slot />
<slot name="footer" />
```

## Methods

Возможно динамическое монтирование окна с передачей ему компонента с данными. Для этого достаточно смонтировать компонент модального окна на уровне `app`, например в `layouts/default.vue` и передавать ему контент для рендера, например:

[Передача компонента с данными](#pass-component)
Передача кода в формате `JSON`
Передача файла в формате `PDF` для просмотра, просто укажите путь к файлу

```js
/** Методы плагина для взаимодейтвия с компонентом */
// Показывает окно с обернутым контентом
$modal.show(modalName)

// Скрывает окно
$modal.hide(modalName, callback)

/** Методы для отображения контента */
// Показывает окно с переданным json
$modal.json(modalName, json)

// Показывает окно с переданным url к pdf, отрендеренному в iframe
$modal.pdf(modalName, src)

// Показывает окно с переданным компонентом и данными
$modal.render({ component, data, props })
```

## Статическое монтирование

### HTML

```vue live
<Modal name="html">
  <p>По умолчанию обернутый контент рендерится в дефотный слот.</p>
</Modal>
```

<Button v-on:click="$modal.show('html')">Показать HTML</Button>

### С параметрами окна

```vue live
<Modal name="html-params" width="400px" height="400px" title="Кастомный тайтл">
  <p>По умолчанию обернутый контент рендерится в дефотный слот.</p>
</Modal>
```

<Button v-on:click="$modal.show('html-params')">Показать с параметрами окна</Button>

## Динамическое монтирование

### Передача компонента

Например, есть некий компонент `<Details />`, который мы хотим отобразить в модальном окне. Передаем этот компонент для рендера модальному окну, предоставить входные параметры для этого компонента `data`, а также изменить входные параметры самого модального окна.

```
<Button @click="handleMountComponent" label="Показать" />
...

<script>
methods: {
  handleMountComponent() {
    const Component = () => import('~/components/ModalExampleComponent')
    this.$modal.render(Component, {
      componentProps: {
        prop1: 'Prop 1...',
        prop2: 'Prop 2...'
      },
      modalProps: {
        title: 'Динамическое монтирование (передача компонента)',
        width: '40rem'
      }
    })
  }
}
</script>
```

<ModalExample v-slot:default="{ handleMountComponent }">
  <Button @click="handleMountComponent" label="Показать" />
</ModalExample>

### Отображение JSON

JSON отображается в нативном `<textarea>`, дабы иметь возможность выделения/редактирования. Выделить весь JSON можно сочетанием клавиш <kbd>Super + A</kbd>

```js
const json = {
  id: 'eaaaf828-e714-4409-ab11-7d51e6086a6a',
  sgtin: '0246071352201034150611303712010A2537B',
  status: 'Ожидает подтверждения получения собственником',
  contractor: {
    name: 'Поставщик Двора Его Императорского Величества, АО',
    inn: '3811029172'
  },
  prod_name: 'Streptocidum',
  sell_name: 'Стрептоцид',
  endDelivery: '0001-01-01T00:00:00',
  sscc: '01946012345678963101000712'
}
```

```
<Button v-on:click="$modal.json(json)">Показать</Button>
```

<Button v-on:click="$modal.json(json)">Показать</Button>

### Отображение PDF

```
<Button v-on:click="$modal.pdf('/mock/sample.pdf')">Показать</Button>
```

<Button v-on:click="$modal.pdf('/mock/sample.pdf')">Показать</Button>
