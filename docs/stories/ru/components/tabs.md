---
title: Tabs
category: components

tabs:
  [
    {
      label: 'Версионирование',
      content: 'Версионирование стилей. Так как мы выбрали путь поставки компонентов по отдельности, а не целой библиотеки компонентов, потребитель может обновить только один пакет, не обновляя другие. В этом случае могут возникнуть конфликты между стилями разных версий компонентов. Поэтому при сборке пакетов в настройках для css-modules прописываем generateScopedName: `[local]-${projectManifest.version}`, где projectManifest — файл package.json. Так у нас решаются все возможные конфликты.',
    },
    {
      label: 'Переиспользование',
      content: 'Переиспользуемые правила. Стараемся дедуплицировать стили и выносить переиспользуемые правила в пакет styles-framework, который включает в себя цвета, типографику, брейкпоинты, сброс стилей и много полезных миксинов. Другие команды могут его использовать в отрыве от библиотеки компонентов.',
    },
    {
      label: 'Кроссбраузерность',
      content: 'Если вы поддерживаете старые версии браузеров и у вас десятки продуктов, вы тоже можете озадачиться такой проблемой, как полифилы. Их вы можете встретить везде: в используемой open-source библиотеке, в пакете, разработанном коллегой из другой команды, и, конечно же, в библиотеке компонентов. В итоге получается, что есть много веб-клиентов, собирающихся с полифилами для стабильной работы во всех браузерах.',
    },
  ]
---

Вкладки облегчают переключение между различными представлениями. Компонент состоит из двух частей, родительского контейнера `<Tabs />` и контейнера для содержимого слота `<Tab />`

## Props

**Tabs**

| Name         | Type     | Default        | Description                                                          |
| ------------ | -------- | -------------- | -------------------------------------------------------------------- |
| onSelect     | Function | return false   | Устанавливает колбэк при открытии.                                   |
| defaultIndex | Number   | 0              | Устанавливает активную вкладку по умолчанию.                         |
| present      | String   | 'button-group' | Меняет представление. Доступны: `classic`, `button-group`, `divided` |

**Tab**

| Name     | Type    | Default   | Description                 |
| -------- | ------- | --------- | --------------------------- |
| title    | String  | undefined | Устанавливает метку кнопки. |
| disabled | Boolean | false     | Делает кнопку неактивной.   |

## Примеры

```
<Tabs>
  <Tab title="Вкладка #1">
    <p>Контент 1</p>
  </Tab>
  <Tab title="Вкладка #2">
    <p>Контент 2</p>
  </Tab>
  <Tab title="Вкладка #3">
    <p>Контент 3</p>
  </Tab>
</Tabs>
```

### Группа кнопок

<box>
<Tabs>
  <Tab v-for="tab in tabs" :key="tab.label" :title="tab.label">
    <p>{{ tab.content }}</p>
  </Tab>
</Tabs>
</box>

**Block**

<box>
<Tabs block>
  <Tab v-for="tab in tabs" :key="tab.label" :title="tab.label">
    <p>{{ tab.content }}</p>
  </Tab>
</Tabs>
</box>

### Отделенные кнопки

<box>
<Tabs present="divided">
  <Tab v-for="tab in tabs" :key="tab.label" :title="tab.label">
    <p>{{ tab.content }}</p>
  </Tab>
</Tabs>
</box>

**Block**

<box>
<Tabs present="divided" block>
  <Tab v-for="tab in tabs" :key="tab.label" :title="tab.label">
    <p>{{ tab.content }}</p>
  </Tab>
</Tabs>
</box>

### Классический вид

<box>
<Tabs present="classic">
  <Tab v-for="tab in tabs" :key="tab.label" :title="tab.label">
    <p>{{ tab.content }}</p>
  </Tab>
</Tabs>
</box>

**Block**

<box>
<Tabs present="classic" block>
  <Tab v-for="tab in tabs" :key="tab.label" :title="tab.label">
    <p>{{ tab.content }}</p>
  </Tab>
</Tabs>
</box>
