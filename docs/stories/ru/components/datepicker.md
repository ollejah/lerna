---
title: Datepicker
category: components
---

**Компонент календаря**
Возращает объект, содержащий интервал дат `{dateStart: '', dateEnd:''}`
Либо, `{date: ''}` в режиме `singleMode`
Формат возвращаемых дат: результат выполнения функции `convertToLocaleString`

## Props

| Name         | Type    | Default       | Description                                             |
| ------------ | ------- | ------------- | ------------------------------------------------------- |
| value        | Object  | {}            | Устанавливает начальный интервал дат.                   |
| pages        | Number  | 2             | Устанавливает количество старниц календаря              |
| singleMode   | Boolean | false         | Устанавливает режим выбора одной даты или интервала дат |
| dateTimeMode | Boolean | false         | Устанавливает режим с выбором времени                   |
| currYear     | Number  | getFullYear() | Устанавливает текущий год                               |
| currMonth    | Number  | getMonth()    | Устанавливает текущий месяц                             |

**Пример**

`<DatePicker />`

```vue live
<DatePicker />
```

## SingleMode

`<DatePicker singleMode />`

```vue live
<DatePicker singleMode />
```

## DateTimeMode

`<DatePicker dateTimeMode />`

```vue live
<DatePicker dateTimeMode />
```
