---
title: Upload
category: components
---

**Компонент загрузки файлов**
Имеет два кейса использования, устанавливаются через `prop` `isButton`:

1. `true` добавление файла/файлов и отправка их на бекенд, используя колбек `onAttach`, и демонстрация
   результата отправки
2. `false` drop-and-down окно с возможностью перетаскивания в него файлов или добавления через диалоговое окно.
   При изменении списка файлов происходит `$emit` события `attach`

## Props

**`isButton = true`**

| Name     | Type     | Default       | Description                                        |
| -------- | -------- | ------------- | -------------------------------------------------- |
| multiple | Boolean  | false         | Режим мультивыбора файлов                          |
| accept   | String   | undefined     | Принимаемые форматы, через запятую                 |
| label    | String   | Выберите файл | Устанавливает текст кнопки.                        |
| color    | String   | undefined     | Устанавливает цвет текста.                         |
| fill     | String   | undefined     | Устанавливает цвет фона и инвертирует цвет текста. |
| size     | String   | undefined     | Устанавливает размер.                              |
| icon     | String   | upload        | Устанавливает иконку.                              |
| onAttach | Function | throw Error   | Устанавливает колбек отправки файлов на бекенд.    |

**`isButton = false`**

| Name     | Type    | Default   | Description                        |
| -------- | ------- | --------- | ---------------------------------- |
| multiple | Boolean | false     | Режим мультивыбора файлов          |
| accept   | String  | undefined | Принимаемые форматы, через запятую |

При добавлении/удалении файлов эмитит событие `attach`

`<Upload accept="png" isButton/>`

```vue live
<Upload accept="xlsx" isButton />
```

`<Upload accept="png"/>`

```vue live
<Upload accept="xlsx" />
```
