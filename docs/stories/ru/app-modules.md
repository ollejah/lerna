---
title: App modules
order: 1
---

## Реализация бизнес-логики приложения

Приложение использует эксперементальную → (**WIP**) структуру моделей данных → когда приложение строится не на основе постоянно изменяющейся бизнес-логики, а на оперируемых приложением сущностях.

Приложение оперирует стандартными _(ранее описанными)_ схемами `schemas` и моделями `models` данных, миксует данные на оновании этих моделей.

Логика операций над данными _(сущностями)_ определяется в моделях → файлах-классах, расположенных в папке `~/models`. Классы схем находятся в папке `~/schemas`.

## Предназначение схем

[Схемы](app-modules/schemas) нужны для трансформации данных необходимых для рендера, в том числе, для предотвращения падений приложения при битых данных.

Файлы схем представляют собой классы, конструктор которых принимает данные и изменяются/обогащаются необходимыми полями.

Микро-разбиение на сущностные схемы позволяет использовать одни и те же схемы во всем приложении и, как в конструкторе, создавать различные наборы данных для рендера, не описывая каждое поле.

## Предназначение моделей

В [моделях](app-modules/models) реализуется логика забора данных, их транформация через схемы, логика пагинация, сохранение и др.

Класс модели использует вспомогательные классы, в которых реализована логика операций с параметрами, пагинации, регистрации моделей, и др. методов и свойств, которые можно использовать в каждой модели, вынеся их в `extends`-класс.

Каждый класс модели получает в конструктор три хелпера `$app`, `$axios` и `$store`, которые могут понадобиться для обеспечения функциональности.

## Инициализация приложения

При загрузке приложения/обновлении страницы инициализируется плагин `nuxt-client-init`, который проверяет есть ли токен в сторе `auth.token` (соотв. сам стор смотрит куку, записанную при авторизации пользователя.

1. Если токен есть — происходит установка токена в заголовок запросов `Authorize Bearer...` и вызывается метод `nuxtClientInit()` в рутовом сторе, который получает данные о пользователе и его правах `auth.getUserInfo()`.
2. Если токена нет — срабатывает редирект на `/login` из `middleware/auth` с последующей авторизацией пользователя `auth.login()`.

**`middleware/auth`**

Проверяет при каждой смене маршрута есть ли токен в сторе `auth.token`. Если токена нет — срабатывает редирект на `/login`, если есть — приложение работает в режиме авторизованного пользователя.

**`auth.login()`**

1. Получение токена
2. Запись токена в куку и стор `auth.token`
3. Вызов метода `auth.getUserInfo()`

**`auth.getUserInfo()`**

Получает данные о пользователе и его права/возможности.

