/* eslint-disable no-console */
import { version as VERSION } from './package.json'

const { ENV, NODE_ENV } = process.env
const PRODUCTION = NODE_ENV === 'production'

async function staticRoutes() {
  const { promisify } = require('util')
  const Glob = require('glob')
  const glob = promisify(Glob)
  const files = await glob('./stories/**/*.{vue,js,md}')
  const routes = files.map(f =>
    f.replace('./', '/').replace(/(.js|.vue|.md)/, '')
  )
  return routes
}

const SITE_NAME = 'ERP FE KIT'

console.log(`
  ${SITE_NAME} | v${VERSION}
  --------------------------
  ENV: ${ENV}
  NODE_ENV: ${NODE_ENV}
`)

export default {
  telemetry: false,
  ssr: !PRODUCTION,
  target: PRODUCTION ? 'static' : 'server',
  // modern: PRODUCTION ? 'client' : false,
  server: { port: 3232 },
  env: {
    VERSION,
    ENV,
    NODE_ENV,
    SITE_NAME,
    LOGGER_LEVEL: 'info',
    PRODUCTION
  },

  loadingIndicator: {
    name: 'three-bounce',
    color: 'hsl(215, 70%, 53%)',
    background: 'white'
  },

  css: ['@ozon-erp/ui-lib/styles/index.scss', 'assets/styles/app.scss'],
  styleResources: {
    scss: [
      '@ozon-erp/ui-lib/styles/config.scss',
      'assets/styles/config.scss',
      'assets/styles/extends.scss'
    ]
  },

  modules: ['@nuxtjs/color-mode'],

  plugins: [
    // '@ozon-erp/ui-lib',
    'plugins/app-components',
    'plugins/app-global'
  ],

  // components: true,

  buildModules: [
    '@nuxtjs/style-resources',
    '@ozon-erp/build-svg-module',
    'modules/nuxt-stories/stories.module'
  ],

  stories: {
    forceBuild: true,
    lang: 'ru',
    staticHost: PRODUCTION
  },

  router: {
    // middleware: ['auth', 'redirect'],
    routeNameSplitter: '/'
  },

  build: {
    transpile: ['@ozon-erp/ui-lib'],

    filenames: {
      app: ({ isDev, isModern }) => {
        return isDev
          ? '[name].js'
          : isModern
          ? 'app/[name].[chunkhash].modern.js'
          : 'app/[name].[chunkhash].legacy.js'
      },
      chunk: ({ isDev, isModern }) => {
        return isDev
          ? '[name].js'
          : isModern
          ? 'js/[name].[chunkhash].modern.js'
          : 'js/[name].[chunkhash].legacy.js'
      },
      css: ({ isDev }) =>
        isDev ? '[name].css' : 'css/[name].[contenthash].css'
    },

    cache: !PRODUCTION,

    parallel: ENV === 'development',
    extractCSS: PRODUCTION,

    postcss: {
      preset: {
        autoprefixer: {},
        sourceMap: true,
        extract: true
      }
    },

    splitChunks: { layouts: true },

    terser: {
      terserOptions: {
        compress: { drop_console: PRODUCTION },
        output: { comments: false },
        safari10: true
      }
    },

    // You can extend webpack config here
    extend(config, ctx) {
      if (!PRODUCTION) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }

      // config.resolve.alias.vue = 'vue/dist/vue.common.js'
      // config.resolve.alias.vue = 'vue/dist/vue.esm.js'

      // config.module.rules.forEach(rule => {
      //   if (rule.test.toString() === '/\\.vue$/') {
      //     rule.options.optimizeSSR = false
      //   }
      // })
    }
  },

  generate: {
    dir: 'dist',
    routes() {
      return staticRoutes()
    }
  }
}
