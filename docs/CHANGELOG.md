# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.9 (2020-12-08)

**Note:** Version bump only for package @ozon-erp/docs





## [1.0.8](https://gitlab.ozon.ru/erp/ozon-erp-fe-kit/compare/docs@1.0.7...docs@1.0.8) (2020-12-08)

**Note:** Version bump only for package docs





## [1.0.7](https://gitlab.ozon.ru/erp/ozon-erp-fe-kit/compare/docs@1.0.6...docs@1.0.7) (2020-12-08)

**Note:** Version bump only for package docs





## [1.0.6](https://gitlab.ozon.ru/erp/ozon-erp-fe-kit/compare/docs@1.0.5...docs@1.0.6) (2020-12-07)

**Note:** Version bump only for package docs





## [1.0.5](https://gitlab.ozon.ru/erp/ozon-erp-fe-kit/compare/docs@1.0.4...docs@1.0.5) (2020-12-07)

**Note:** Version bump only for package docs





## 1.0.4 (2020-12-07)

**Note:** Version bump only for package docs





## [1.0.3](https://gitlab.ozon.ru/erp/ozon-erp-fe-kit/compare/docs@1.0.2...docs@1.0.3) (2020-12-07)

**Note:** Version bump only for package docs





## [1.0.2](https://gitlab.ozon.ru/erp/ozon-erp-fe-kit/compare/docs@1.0.1...docs@1.0.2) (2020-12-07)

**Note:** Version bump only for package docs





## 1.0.1 (2020-12-07)

**Note:** Version bump only for package docs
