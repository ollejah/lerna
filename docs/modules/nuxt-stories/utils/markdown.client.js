// import DOMPurify from 'dompurify' // TBD: re-add. Since we compile vue on-the-fly, sanitizing will break that. (may be ok since it's a dev-only tool).
import Prism from 'prismjs'
import matter from 'gray-matter'
import marked from 'marked'

marked.setOptions({
  gfm: true,
  tables: true,
  breaks: true,
  pedantic: false,
  sanitize: false,
  smartLists: true,
  smartypants: false
  // highlight: function (code) {
  //   return hljs.highlightAuto(code).value
  // }
})

const renderer = new marked.Renderer()

// Renderer Overrides:
renderer.code = (code, language) => {
  let lang = language.split('|')[0] || language
  const filename = language.split('|')[1] || ''

  const live = /live/.test(lang)

  lang = lang.split(' ')[0] || lang

  lang = lang.split(' ')[0] || lang
  lang = lang === 'vue' ? 'html' : lang
  lang = lang === 'bash' ? 'clike' : lang
  lang = lang === 'json' ? 'js' : lang

  if (!lang) lang = 'html'

  if (live) {
    return `<editor-live code='${code}' lang='${lang}'></editor-live>`
  } else {
    const html = Prism.highlight(code, Prism.languages[lang], lang)
    return `<code-block filename="${filename}"><pre><code class="${lang}">${html}</code></pre></code-block>`
  }
}

/* renderer.html = html => {
  console.log(html)
  const RE = /<script\b[^>]*>([\s\S]*?)<\/script>/gm

  if (html.match(RE)) {
    const matches = RE.exec(html)
    console.log(matches[1].trim())
    return `<script>${matches[1].trim()}</script>`
    // return `Q${matches[1].trim()}Q`
  }

  return html
} */

/* const walkTokens = token => {
  // const REGEX = /<script.*?>([\s\S]*?)<\/script>/gim
  const RE = /<script\b[^>]*>([\s\S]*?)<\/script>/gm

  if (token.type === 'html' && token.raw.match(RE)) {
    const matches = RE.exec(token.raw)
    console.log(matches[1].trim())
    token.pre = undefined
    console.log(token)
    // return `<script>${matches[1].trim()}</script>`
    return token
  }
  // return false
}

marked.use({ walkTokens }) */

// renderer.heading = (text, level) => {
//   const escapedText = text
//     .toLowerCase()
//     .split(' ')
//     .join('-')
//   return `
//   <h${level} id="${escapedText}" class="toc-target">
//     <a href="#${escapedText}" class="anchor">#</a>
//     ${text}
//   </h${level}>`
// }

const mdOptions = { renderer }

const Markdown = {
  parseTOC(tokens) {
    const hdrs = tokens
      .filter(({ type, depth }) => type === 'heading' && depth <= 3)
      .map(({ text, depth }) => {
        const href =
          '#' +
          text
            .toLowerCase()
            .split(' ')
            .join('-')
        return { href, text, depth }
      })
    return hdrs
  },
  parse(input) {
    let parsedMatter
    try {
      parsedMatter = matter(input)
    } catch (err) {
      parsedMatter = { content: input, data: {} }
    }
    const { content, data: frontMatter } = parsedMatter
    // const md = content
    const lexer = new marked.Lexer()
    const tokens = lexer.lex(content)
    const toc = Markdown.parseTOC(tokens)
    const compiled = marked(content, mdOptions)
    return { toc, md: content, compiled, frontMatter }
  }
}

export default Markdown
