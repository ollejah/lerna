import Vue from 'vue'

/**
 * Vue plugin
 * FIXME
 * Имеет смысл отрефачить в рендер-функцию,
 * будет легче прокинуть пропсы и будет быстрее
 */
import BreadCrumbs from './BreadCrumbs.vue'

const BreadCrumbsPlugin = {
  install(Vue, options) {
    Object.assign(BreadCrumbs.pages, options.pages)
    Vue.component('BreadCrumbs', BreadCrumbs)
  }
}

/**
 * Nuxt plugin
 */
export default async ({ app }) => {
  const routes = app.router.options.routes

  const list = routes.map(async route => {
    const { title } = await route.component()
    return { [route.name]: title }
  })

  const map = await Promise.all(list)

  const pages = map.reduce((acc, item) => {
    return { ...acc, ...item }
  }, {})

  Vue.use(BreadCrumbsPlugin, { pages })
}
