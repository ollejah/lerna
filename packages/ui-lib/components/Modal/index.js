import Vue from 'vue'
import Modal from './Modal.vue'
Vue.component('Modal', Modal)

const plugin = {
  install(Vue, options = {}) {
    if (this.installed) return

    this.installed = true
    this.event = new Vue()

    const Component = Vue.extend(Modal)
    const root = new Component()
    if (process.client) {
      root.$mount(document.body.appendChild(document.createElement('div')))
    }

    // Методы плагина для взаимодейтвия с компонентом
    this.show = name => {
      console.log('Modal show', name, `toggle:${name}`)
      this.event.$emit(`toggle:${name}`, true)
    }
    this.hide = (name, callback) => {
      this.event.$emit(`toggle:${name}`, false, callback)
    }

    // Vue.component('Modal', Modal)
    Object.assign(this, root)
  }
}

// Vue.use(plugin)

export default plugin

/** For use into Nuxt, create plugin */
// export default (ctx, inject) => {
//   inject('message', plugin)
// }
