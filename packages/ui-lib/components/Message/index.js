// import Vue from 'vue'
import Message from './Message.vue'

/**
 * Message toast plugin
 * this.$message.info(message)
 * this.$message.success(message)
 * this.$message.warning(message)
 * this.$message.error(message)
 */
const plugin = {
  install(Vue, options = { delay: 3000, order: 'desc' }) {
    if (this.installed) return

    this.installed = true

    const Component = Vue.extend(Message)
    const root = new Component({ propsData: options })
    if (process.client) {
      root.$mount(document.body.appendChild(document.createElement('div')))
    }

    Vue.component('Message', root)
    Object.assign(this, root)
  }
}

// Vue.use(plugin, { delay: 3000, order: 'desc' })

export default plugin

/** For use into Nuxt, create plugin */
// export default (ctx, inject) => {
//   inject('message', plugin)
// }
