import { isBetween, isEqual, isMore } from './utils'

const NUM_DAYS_IN_WEEK = 7
const NUM_WEEKS_IN_MONTH = 6

export default class DateService {
  yearList = []
  monthList = []
  hours = [...Array(24).keys()]
  minutes = [...Array(60).keys()]
  year = undefined
  month = undefined
  dateSingle = undefined
  dateStart = undefined
  dateEnd = undefined
  dateFocused = undefined
  isAnotherMonth = false
  direction = 1
  singleMode = false
  dateTimeMode = false

  constructor(singleMode = false, dateTimeMode = false) {
    this.singleMode = singleMode
    this.dateTimeMode = dateTimeMode
  }

  // Год для каждой страницы календаря
  setYearList(currYear, pages) {
    const yearList = []
    for (let i = 0; i < pages; i++) {
      yearList[i] = currYear
    }
    this.yearList = yearList
  }

  // Месяц для каждой страницы календаря
  setMonthList(currMonth, pages) {
    const monthList = []
    for (let i = 0; i < pages; i++) {
      let month = null
      if (currMonth + i === 12) {
        month = 0
        this.yearList[i]++
      } else {
        month = currMonth + i
      }
      monthList[i] = month
    }
    this.monthList = monthList
  }

  // Первая неделя месяца
  getFirstWeek(lastDayPreviousMonth, numDaysInFirstWeek) {
    this.month--
    this.isAnotherMonth = true
    const prevMonthWeek = this.generateWeek(
      lastDayPreviousMonth - NUM_DAYS_IN_WEEK + numDaysInFirstWeek + 1,
      NUM_DAYS_IN_WEEK - numDaysInFirstWeek
    )
    this.month++
    this.isAnotherMonth = false
    const currMonthWeek = this.generateWeek(1, numDaysInFirstWeek)
    return [...prevMonthWeek, ...currMonthWeek]
  }

  // Недели между первой и последней
  getMiddleWeeks(numDays, numDaysInFirstWeek, numDaysInLastWeek) {
    this.isAnotherMonth = false
    const startDate = numDaysInFirstWeek + 1
    const endDate = numDays - numDaysInLastWeek
    const middleWeeks = []
    for (let i = startDate; i < endDate; i += NUM_DAYS_IN_WEEK) {
      middleWeeks.push(this.generateWeek(i, NUM_DAYS_IN_WEEK))
    }
    return middleWeeks
  }

  // Последняя неделя месяца
  getLastWeek(numDays, numDaysInLastWeek, numMiddleWeeks) {
    let mainWeek = []
    const additionalWeek = []
    this.isAnotherMonth = false
    const currMonthWeek = this.generateWeek(
      numDays - numDaysInLastWeek + 1,
      numDaysInLastWeek
    )
    this.month++
    this.isAnotherMonth = true
    const nextMonthWeek = this.generateWeek(
      1,
      NUM_DAYS_IN_WEEK - numDaysInLastWeek
    )
    mainWeek = [...currMonthWeek, ...nextMonthWeek]
    for (let i = 0; i < NUM_WEEKS_IN_MONTH - numMiddleWeeks - 2; i++) {
      const startDate =
        i === 0
          ? NUM_DAYS_IN_WEEK - numDaysInLastWeek + 1
          : NUM_DAYS_IN_WEEK + 1
      const week = this.generateWeek(startDate, NUM_DAYS_IN_WEEK)
      additionalWeek.push(week)
    }
    return [mainWeek, ...additionalWeek]
  }

  // Формирование недели
  generateWeek(startDate, numDays, weekCountPrefix = 0) {
    const week = []
    for (let i = 0; i < numDays; i++) {
      const value = new Date(this.year, this.month, startDate + i)
      const { equal, between } = this.checkIsSelected(value)
      const needHover = this.checkNeedHover(value)
      week[weekCountPrefix + i] = {
        value,
        equal,
        between,
        needHover: needHover && !this.isAnotherMonth,
        isAnotherMonth: this.isAnotherMonth
      }
    }
    return week
  }

  // Находится ли текущая дата в интервале выбранных
  checkIsSelected(date) {
    if (this.dateSingle)
      return {
        equal: isEqual(new Date(date), new Date(this.dateSingle)),
        between: false
      }
    const equal =
      isEqual(new Date(date), new Date(this.dateStart)) ||
      isEqual(new Date(date), new Date(this.dateEnd))

    const between = isBetween(
      new Date(date),
      new Date(this.dateStart),
      new Date(this.dateEnd)
    )
    return { equal, between }
  }

  // Нужно ли подсвечивать дату между датой старта и датой под курсором мыши
  checkNeedHover(date) {
    if (!this.dateStart || (this.dateStart && this.dateEnd)) return false
    return (
      isBetween(
        new Date(date),
        new Date(this.dateStart),
        new Date(this.dateFocused)
      ) ||
      isBetween(
        new Date(date),
        new Date(this.dateFocused),
        new Date(this.dateStart)
      )
    )
  }

  // Перенос воскресенья в конец, поскольку по дефолту неделя стартует с воскресенья
  convertSunday(day) {
    return day === 0 ? 6 : day - 1
  }

  // Массив дней в месяце на основании сформированных недель
  getDaysPerMonth(cnt) {
    this.year = this.yearList[cnt]
    this.month = this.monthList[cnt]
    const firstDay = this.convertSunday(
      new Date(this.year, this.month, 1).getDay()
    )
    const lastDay = this.convertSunday(
      new Date(this.year, this.month + 1, 0).getDay()
    )
    const lastDayPreviousMonth = new Date(this.year, this.month, 0).getDate()

    const numDays = new Date(this.year, this.month + 1, 0).getDate()
    const numDaysInFirstWeek = NUM_DAYS_IN_WEEK - firstDay
    const numDaysInLastWeek = lastDay + 1
    const firstWeek = this.getFirstWeek(
      lastDayPreviousMonth,
      numDaysInFirstWeek
    )
    const middleWeeks = this.getMiddleWeeks(
      numDays,
      numDaysInFirstWeek,
      numDaysInLastWeek
    )
    const numMiddleWeeks = middleWeeks.length
    const lastWeek = this.getLastWeek(
      numDays,
      numDaysInLastWeek,
      numMiddleWeeks
    )
    return [firstWeek, ...middleWeeks, ...lastWeek]
  }

  // Пересчет месяца и года для текущей страницы календаря при нажатии на стрелки/ выборе из выпадающего списка
  balanceMonths(monthList, yearList) {
    const { direction } = this
    for (let i = 0; i < monthList.length; i++) {
      // Месяц вперед
      if (
        monthList[i] >= monthList[i + 1] &&
        yearList[i + 1] === yearList[i] &&
        direction === 1
      ) {
        if (monthList[i + 1] === 11) {
          monthList[i + 1] = 0
          yearList[i + 1] = yearList[i + 1] + 1
        } else {
          monthList[i + 1] = monthList[i + 1] + 1
        }
      }
      // Месяц назад
      if (
        monthList[i] >= monthList[i + 1] &&
        yearList[i] === yearList[i + 1] &&
        direction === -1
      ) {
        if (monthList[i] - 1 < 0) {
          monthList[i] = 11
          yearList[i] = yearList[i + 1] - 1
        } else {
          monthList[i] = monthList[i + 1] - 1
        }
      }
    }
    return { monthList, yearList }
  }

  balanceYears(yearList, monthList) {
    const { direction } = this
    for (let i = 0; i < yearList.length - 1; i++) {
      const postFix = monthList[i] === 11 ? 1 : 0
      if (yearList[i] > yearList[i + 1] && direction === -1)
        yearList[i] = yearList[i + 1] + postFix * direction
      if (yearList[i] > yearList[i + 1] && direction === 1)
        yearList[i + 1] = yearList[i] + postFix * direction
    }
    for (let i = 0; i < monthList.length - 1; i++) {
      if (monthList[i] > monthList[i + 1] && yearList[i] === yearList[i + 1]) {
        monthList[i] = monthList[i + 1] - 1
      }
    }
    return { yearList, monthList }
  }

  // Выбор дат
  selectDates(date) {
    if (this.singleMode) {
      this.dateSingle = date
      return {
        complete: true
      }
    }
    if (!this.dateStart) {
      this.dateStart = date
    } else if (this.dateStart && this.dateEnd) {
      this.dateStart = date
      this.dateEnd = undefined
    } else if (isMore(this.dateStart, date)) {
      this.dateEnd = this.dateStart
      this.dateStart = date
    } else {
      this.dateEnd = date
      if (
        this.dateStart.getTime() === this.dateEnd.getTime() &&
        this.dateTimeMode
      ) {
        this.dateEnd.setHours(23, 59, 59, 999)
      }
    }
    return {
      complete: this.dateStart && this.dateEnd
    }
  }

  // Время даты окончания: 23:59, если даты начала и окончания совпадают
  balanceTime() {}

  // Переключение месяца
  toggleMonth(page, month, year = this.yearList[page]) {
    const monthList = [...this.monthList]
    const yearList = [...this.yearList]

    this.direction = month > monthList[page] || year > yearList[page] ? 1 : -1
    let newMonth = month
    let newYear = year
    if (newMonth < 0) {
      newMonth = 11
      newYear -= 1
    }
    if (newMonth > 11) {
      newMonth = 0
      newYear += 1
    }
    if (newYear >= 1970) {
      let updatedMonthList = monthList
      let updatedYearList = yearList
      if (month !== monthList[page]) {
        monthList[page] = newMonth
        yearList[page] = newYear
        const result = this.balanceMonths(monthList, yearList)
        updatedMonthList = result.monthList
        updatedYearList = result.yearList
      }
      if (year !== yearList[page]) {
        yearList[page] = newYear
        const result = this.balanceYears(yearList, updatedMonthList)
        updatedMonthList = result.monthList
        updatedYearList = result.yearList
      }
      this.monthList = updatedMonthList
      this.yearList = updatedYearList
    }
  }

  // Сбросить выбранные даты
  clearState() {
    if (this.singleMode) {
      this.dateSingle = undefined
      return { date: undefined }
    } else {
      this.dateStart = undefined
      this.dateEnd = undefined
      return { dateStart: undefined, dateEnd: undefined }
    }
  }
}
