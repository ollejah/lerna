export const isBetween = (date, min, max) =>
  date.getTime() > min.getTime() && date.getTime() < max.getTime()

export const isEqual = (date1, date2) => date1.getTime() === date2.getTime()

export const isMore = (date1, date2) => date1.getTime() > date2.getTime()

export const convertToLocaleString = (date, withTime = false) => {
  if (!date) return undefined
  const params = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  }
  if (withTime) Object.assign(params, { hour: '2-digit', minute: '2-digit' })
  return new Date(date).toLocaleString('ru', params)
}
