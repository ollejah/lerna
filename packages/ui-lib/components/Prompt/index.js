import Vue from 'vue'
import Prompt from './Prompt.vue'

/**
 * Prompt plugin
 * this.$prompt.show({ title: String, content: String, accept: Function, decline: Function })
 */
const plugin = {
  install(Vue, options = {}) {
    if (this.installed) return

    this.installed = true

    const Component = Vue.extend(Prompt)
    const root = new Component({ propsData: options })
    if (process.client) {
      root.$mount(document.body.appendChild(document.createElement('div')))
    }

    Vue.component('Prompt', root)
    Object.assign(this, root)
  }
}

Vue.use(plugin)

export default plugin

/** For use into Nuxt, create plugin */
// export default (ctx, inject) => {
//   inject('message', plugin)
// }
