// https://material.io/resources/icons/?icon=delete_outline&style=sharp

const files = require.context('./', false, /.svg$/)
export default files
  .keys()
  .map(files)
  .map(item => item.default.id)

/* const dictionary = {}

files.keys().forEach(path => {
  const file = path.replace('./', '')
  const key = file.replace('.svg', '')
  const content = async () => await import(`./${file}`)

  try {
    dictionary[key] = atob(content.split(',').pop())
  } catch (e) {
    dictionary[key] = content
  }
})

console.log('dictionary', dictionary)
export { dictionary } */
