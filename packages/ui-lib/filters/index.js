import Vue from 'vue'

import { date, time, datetime, timewsec } from '~/utils/dates'
Vue.filter('date', date)
Vue.filter('time', time)
Vue.filter('timewsec', timewsec)
Vue.filter('datetime', datetime)

// import { fioSquash } from '~/utils/fio-squash'
// Vue.filter('fio', fioSquash)

// import sum from '~/utils/currency'
// Vue.filter('sum', sum)
