// import svg from '../icons/info-outline.svg'
// const icon = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="${svg.viewBox}" width="16" height="16" class="icon" fill="currentColor"><use xlink:href="#${svg.id}" /></svg>`

export default {
  inserted(el, binding, vnode) {
    if (!el.textContent.trim() === '' && !binding.modifiers.content) return

    const hint = document.createElement('div')
    hint.classList.add('v-hint')

    if (binding.modifiers.content) {
      hint.insertAdjacentHTML('beforeend', binding.value)
    } else {
      hint.insertAdjacentHTML('beforeend', el.textContent)
    }

    // если нужна иконка
    // hint.insertAdjacentHTML('beforeend', `${icon} ${el.textContent}`)

    el.mouseenterHandler = e => {
      e.stopPropagation()

      // устанавливаем координаты элементу, не забываем про "px"
      const { top, left } = el.getBoundingClientRect()
      hint.style.cssText = `top: ${top}px; left: ${left}px;`

      document.body.appendChild(hint)

      // двигаем тултип влево, если вылезает за правую границу экрана
      const { right } = hint.getBoundingClientRect()
      const viewportWidth = window.innerWidth - 16
      if (right > viewportWidth) {
        hint.style.cssText += `margin-left: -${right - viewportWidth}px`
      }

      setTimeout(() => hint.classList.add('is-active'), 10)

      if (hint) setTimeout(() => el.mouseleaveHandler(), 5000)
    }

    el.mouseleaveHandler = e => {
      hint.classList.remove('is-active')
      setTimeout(() => hint.remove(), 150)
    }

    el.addEventListener('mouseenter', el.mouseenterHandler)
    el.addEventListener('mouseleave', el.mouseleaveHandler)
  },
  unbind(el) {
    el.removeEventListener('mouseenter', el.mouseenterHandler)
    el.removeEventListener('mouseleave', el.mouseleaveHandler)
  }
}
