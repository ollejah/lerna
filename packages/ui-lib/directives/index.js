/* eslint-disable import/first */
import Vue from 'vue'

import clickoutside from './clickoutside'
Vue.directive('click-outside', clickoutside)

import lazyload from './lazyload'
Vue.directive('lazyload', lazyload)

import focus from './focus'
Vue.directive('focus', focus)

import copy from './copy'
Vue.directive('copy', copy)

import external from './link-external'
Vue.directive('link-external', external)

import hint from './hint'
Vue.directive('hint', hint)

export { clickoutside, lazyload, focus, copy, external, hint }
