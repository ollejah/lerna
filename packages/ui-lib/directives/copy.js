import copy from '../utils/copy'

import svg from '../icons/copy.svg'
const icon = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="${svg.viewBox}" width="16" height="16" class="icon"><use xlink:href="#${svg.id}"></use></svg>`

export default {
  inserted(el, binding, vnode) {
    const documentHandler = e => {
      e.stopPropagation()
      const text = el.textContent
      try {
        // NOTE Это было бы идеально
        // navigator.clipboard.writeText(text)

        copy(text)
        vnode.context.$Message.info('Значение скопировано')
      } catch (err) {
        alert('Error! could not copy text', err)
      }
    }
    el.__vueCopy__ = documentHandler
    el.__trigger__ = null

    if (binding.modifiers.outside) {
      const trigger = document.createElement('ins')
      trigger.classList.add('v-copy-outside')
      trigger.setAttribute('hint', 'Нажмите, чтобы скопировать')
      trigger.insertAdjacentHTML('beforeend', icon)
      el.parentElement.insertAdjacentElement('afterend', trigger)
      trigger.addEventListener('click', documentHandler)
      el.__trigger__ = trigger
    } else {
      el.classList.add('v-copy')
      el.setAttribute('hint', 'Нажмите, чтобы скопировать')
      el.addEventListener('click', documentHandler)
      el.__trigger__ = el
    }
  },
  unbind(el) {
    el.__trigger__.removeEventListener('click', el.__vueCopy__)
    delete el.__vueCopy__
    delete el.__trigger__
  }
}
