import svg from '../icons/link-external.svg'
const icon = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="${svg.viewBox}" width="16" height="16" class="icon"><use xlink:href="#${svg.id}"></use></svg>`

export default {
  inserted: (el, binding) => {
    el.target = '_blank'
    el.rel = 'noopener noreferrer'
    el.className = 'v-link-external'
    el.innerHTML = `<span>${el.innerHTML}</span>`

    if (binding.modifiers.before) {
      // v-link-external.before
      el.insertAdjacentHTML('afterbegin', icon)
    } else {
      // v-link-external.after
      el.insertAdjacentHTML('beforeend', icon)
    }
  }
}
