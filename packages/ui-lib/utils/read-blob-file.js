// import axios from 'axios'

/**
 * Получает файл, парсит тип контента, генерит блоб
 * создает DOMString, содержащий URL с указанием на объект
 * https://developer.mozilla.org/ru/docs/Web/API/URL/createObjectURL
 *
 * Поскольку все запросы к API должны быть с токеном,
 * по прямому HTTP-запросу получить файл невозможно, только через $axios
 *
 * NOTE: Для взаимодействия с контекстом приложения после импорта положить в методы
 *
 * @param {String} url Путь к ручке в API gateway
 * @param {Object} params Параметры запроса
 */
export default async function readBlobFile(url, params = {}) {
  try {
    const res = await window.$nuxt.$axios(url, {
      params,
      responseType: 'blob',
      errorHandle: false,
      onDownloadProgress: ({ loaded, total }) => {
        const progress = parseInt(Math.round((loaded / total) * 100))
        window.$nuxt.$emit('progress', progress)
      }
    })

    const type = res.headers['content-type']
    const blob = new Blob([res.data], { type })

    const reader = new FileReader()
    return new Promise((resolve, reject) => {
      reader.onerror = () => {
        reader.abort()
        reject(new DOMException('Problem parsing input file.'))
      }
      reader.onload = () => resolve(reader.result)
      reader.readAsDataURL(blob)
    })
  } catch (error) {
    // console.warn(error)
    throw new Error({ statusCode: 404, message: 'Что-то пошло не так' })
  }
}
