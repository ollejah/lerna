export default function copy(text) {
  const cssText = 'position:fixed;pointer-events:none;z-index:-9999;opacity:0;'
  const textArea = document.createElement('textarea')

  textArea.value = text
  textArea.style.cssText = cssText
  document.body.appendChild(textArea)
  textArea.select()
  document.execCommand('copy')
  textArea.parentNode.removeChild(textArea)
}
