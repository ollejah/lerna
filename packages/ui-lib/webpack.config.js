const { resolve } = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// const CopyWebpackPlugin = require('copy-webpack-plugin')
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin')

// https://vue-loader.vuejs.org/ru/guide/linting.html#eslint
// https://vue-loader.vuejs.org/ru/guide/linting.html#stylelint

const mode = 'production'

const cssExtract = mode =>
  mode !== 'production'
    ? 'vue-style-loader'
    : {
        loader: MiniCssExtractPlugin.loader
        // options: { esModule: true }
      }

// https://github.com/svg/svgo
const svgo = {
  plugins: [
    { removeSVGTagAttrs: false },
    { removeViewBox: false },
    { removeDoctype: true },
    { removeComments: true }
  ]
}

module.exports = {
  mode,
  stats: {
    assets: true,
    children: false,
    chunks: false,
    hash: false,
    modules: false,
    publicPath: false,
    timings: true,
    version: false,
    warnings: true
  },
  devtool: '#source-map',

  // target: 'web',
  entry: resolve(__dirname, './src/index.js'),
  output: {
    path: resolve(__dirname, './src/dist'),
    filename: 'index.js',
    library: 'ui-lib',
    libraryTarget: 'umd'
  },

  externals: {
    vue: 'vue'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader'
      },
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [cssExtract(mode), 'css-loader']
      },
      {
        test: /\.s(a|c)ss$/,
        use: [
          cssExtract(mode),
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } },
          {
            loader: 'sass-resources-loader',
            options: {
              sourceMap: true,
              resources: [
                resolve(__dirname, './src/styles/config.scss')
                // resolve(__dirname, './src/styles/extends/_extends.scss')
              ]
            }
          }
        ]
      },
      /* {
        test: /\.svg$/,
        include: /svg/,
        use: [
          'babel-loader',
          {
            loader: 'vue-svg-loader',
            options: { svgo }
          }
        ]
      }, */
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-sprite-loader',
            options: {
              extract: true
              // spriteFilename: 'sprite.[hash:8].svg'
            }
          },
          {
            loader: 'svgo-loader',
            options: { svgo }
          }
        ]
      }
    ]
  },

  // optimization: {},

  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({ filename: 'styles.css' }),
    new SpriteLoaderPlugin({ plainSprite: true })
    // new CopyWebpackPlugin(
    //   [
    //     {
    //       from: resolve(__dirname, '../src'),
    //       to: '../src'
    //     }
    //   ],
    //   {
    //     ignore: ['.*']
    //   }
    // )
  ],

  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm.js',
      '@': resolve(__dirname, './'),
      '~': resolve(__dirname, './')
    },
    extensions: ['.js', '.vue', '.json']
  }
}
