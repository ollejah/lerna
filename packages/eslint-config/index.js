module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'prettier',
    'prettier/vue',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended'
    // 'plugin:sonarjs/recommended'
  ],
  plugins: [
    'prettier'
    // 'sonarjs'
  ],
  // add your custom rules here
  rules: {
    'no-console': 'off',
    'no-useless-return': 'off',
    'vue/valid-v-bind': 'off',
    'accessor-pairs': 'off',
    'prettier/prettier': 'off'
  }
}
