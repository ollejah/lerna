# @ozon-erp/eslint-config

**Install**

```
npm i -D eslint @ozon-erp/eslint-config
```

**Usage**

https://eslint.org/docs/2.0.0/user-guide/configuring

Create file `.eslintrc.js` in the root of your project like this:

```
module.exports = {
  "extends": "@ozon-erp/eslint-config",
}
```
