# `@ozon-erp/app`

Application Nuxt.js module for Ozon ERP Frontend Kit.


## Usage

(nuxt.config.js)

```js 
  app: {
   
  },
  modules: [
    '@ozon-erp/app'
  ]
```
