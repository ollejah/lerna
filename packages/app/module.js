import { resolve } from 'path'

export { default as headMeta } from './head-meta'
export { default as axiosConfig } from './axios-config'
export { default as App } from './lib/App'
export { default as Store } from './lib/Store'
export { default as State } from './lib/State'
export { default as BaseModel } from './lib/BaseModel'
export { default as Params } from './lib/Params'
export { default as Events } from './lib/Events'
export { default as Queue } from './lib/Queue'

export default function ApplicationModule(_moduleOptions) {
  const { nuxt } = this

  const options = {
    ...nuxt.options.app,
    ..._moduleOptions,
    ...(nuxt.options.runtimeConfig && nuxt.options.runtimeConfig.app)
  }

  if (!options.models) {
    // Укажите путь к моделям в опциях модуля в 'nuxt.config.js'
    options.models = '~/models/'
  }

  this.addPlugin({
    src: resolve(__dirname, 'plugin.js'),
    options
  })

  // NOTE Регистрируем мидвару как плагин
  this.addPlugin({
    src: resolve(__dirname, 'init.js'),
    fileName: './init.js',
    options
  })

  // Добавляем мидвару как мидлвару в контекст Nuxt
  this.options.router.middleware.push('init')

  if (options.auth) {
    this.requireModule([
      '@ozon-erp/auth',
      typeof options.auth === 'object' ? options.auth : {}
    ])
  }
  this.requireModule(['@nuxtjs/axios'])
}

// module.exports.meta = require('./package.json')
