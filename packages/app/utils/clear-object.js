/**
 * Очищает объект от NULL, undefined, пустой строки
 * @param {Object} obj
 */
export default function clearObject(obj) {
  return Object.keys(obj).reduce((acc, key) => {
    const item = obj[key] ? { [key]: obj[key] } : {}
    return { ...acc, ...item }
  }, {})
}
