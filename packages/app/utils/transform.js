/**
 * Трансформирует данные согласно формату для рендера
 *
 * Устанавливает стандарт, упрощает взаимодействие view-слоя
 * с данными, полученными от серверного API внутри приложения
 *
 * @param {Object | Array} data Данные для обработки
 * @param {Constructor} schema Схема данных для рендера
 */
export default function transform(data, Schema) {
  data.items && (data = data.items)

  if (!Schema) {
    return Promise.resolve(data)
  }

  if (data.constructor.name === 'Array') {
    return Promise.resolve(data.map(el => new Schema(el)))
  } else {
    return Promise.resolve(new Schema(data))
  }
}
