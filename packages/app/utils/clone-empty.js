/**
 * Клонирует объект с пустыми значениями
 * @param {Object} obj
 */
export default function cloneEmpty(obj) {
  return JSON.parse(
    JSON.stringify(obj, (key, value) => (value === undefined ? '' : value))
  )
}
