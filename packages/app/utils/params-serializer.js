/**
 * Изменяет принцип сериализации параметров
 *
 * для корректной обработки бэкэнда массивов
 * в `axios` при использовании опции `paramsSerializer`
 *
 * Изменяет `prop[0]=value1&prop[1]=value2` => `prop=value1&prop=value2`
 *
 * @param {Object} params
 */
export default function serializeParams(params) {
  const keys = Object.keys(params)
  let options = ''

  keys.forEach(key => {
    const isObject = typeof params[key] === 'object'
    const isArray = isObject && params[key].length > 0

    if (!isObject && params[key]) {
      const val = encodeURIComponent(params[key])
      options += `${key}=${val}&`
    }

    if (isObject && isArray) {
      params[key].forEach(val => {
        val = encodeURIComponent(val)
        if (val) options += `${key}=${val}&`
      })
    }
  })

  return options ? options.slice(0, -1) : options
}
