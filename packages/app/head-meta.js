/**
 * App Head Meta
 * Устанавливает заголовки страниц <title> на основе маршрутизации
 */
export default async (env, route, page, prefixes) => {
  /** Читаем установки из компонента страницы */
  const { title, space } = await page

  const section = (() => {
    // На страницах первого уровня вернем как есть
    if (!space) return `%s`

    const dir = space.toLowerCase()
    return route.path.includes(dir) ? prefixes[dir] : `%s`
  })()

  // На главной выводит просто название сайта
  const isMainPage = title === env.SITE_NAME
  
  // Запоняет шаблон
  const titleTemplate = isMainPage ? null : `${section} | ${env.SITE_NAME}`
  
  // Устанавливает опцию
  page.head = { title, titleTemplate }
}
