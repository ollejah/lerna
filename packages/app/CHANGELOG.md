# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.2.3 (2020-12-22)

**Note:** Version bump only for package @ozon-erp/app
