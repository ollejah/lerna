import Vue from 'vue'
import { headMeta } from '@ozon-erp/app'
import middleware from './middleware'

/**
 * App Init Middleware
 * Это магическая миддлвара, активирующая модель для текущей страницы
 * с последующим резолвом прав, статуса авторизации и биндинга во вью-слой
 *
 * NOTE: Сейчас активируется в `module.js`
 * добавлением в `options.router.middleware`
 *
 * использование `addTemplate` копирует в `~/middleware`,
 * что увеличивает риск исправления и перезаписи при старте билда
 *
 * Можно рассмотреть билд-хуки
 */
middleware.init = async function(ctx) {
  const { env, $App, $auth, redirect, route } = ctx

  /** Получаем опции из компонента страницы */
  const page = await route.matched[0].components.default.options

  let prefixes = {}

  <% if (options.prefixes) { %>
  prefixes = <%= JSON.stringify(options.prefixes) %>
  <% } %>

  /** Устанавим заголовки страниц <title> */
  await headMeta(env, route, page, prefixes)

  /** Читаем опции из компонента страницы */
  const { space, access, model } = page

  /** Устанавливает активный спейс (раздел приложения для ACL) */
  $App.activeSpace = space

  /** Устанавливает активную модель для страницы */
  if (model) {
    // Установим шоркат для модели
    const $model = $App[model]

    // NOTE for debug
    if (process.client && process.env.ENV != 'production') window.model = $model

    // Запишем имя модели, т.к. имена классов обфусцируются
    $model.NAME = model

    // Если у модели есть init(), запускаем его при активации
    // Во избежание повторных запросов из init()
    // установим флаг активации
    if ($model.init && !$model?.activated) {
      await $model.init()
      console.info(`${model} inited`)
      $model.activated = true
    }

    // Забиндим модель для Vue и Nuxt контекстов
    Vue.prototype.$model = $model
    ctx.$model = $model
  }

  /**
   * ACL (access control list)
   * Управление доступом к страницам
   * https://www.imperva.com/learn/data-security/role-based-access-control-rbac/
   */
  if ($auth && access) {
    const { permissions } = $auth
    if (!permissions[access]?.Access) redirect('/')
  }
}
