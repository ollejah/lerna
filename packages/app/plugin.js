import Vue from 'vue'
import { App, Events, axiosConfig } from '@ozon-erp/app'

/**
 * Require all models on the path and with the pattern defined
 * https://webpack.js.org/guides/dependency-management/#requirecontext
 */
const req = require.context(`<%= options.models %>`, true, /index.js$/)
const map = req.keys().map(req)
const models = map.reduce((acc, item) => ({ ...acc, ...item }), {})

export default async (ctx, inject) => {
  // Inject EventBus
  const bus = new Vue()
  inject('bus', bus)
  ctx.$bus = bus

  const events = new Events()
  inject('events', events)
  ctx.$events = events

  // Create App namespace models
  const app = new App(ctx, models)

  // Inject App
  inject('App', app)
  ctx.$App = app

  // For debuging
  if (process.client && process.env.ENV != 'production') window.app = app

  // Setup Axios config
  axiosConfig(ctx)

  // Authorize user and get user info before init app
  ctx.$auth && (await ctx.$auth.init())
  ctx.$events.emit('AppInit', ctx)
}
