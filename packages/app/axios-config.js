export default (ctx) => {
  const { env, $axios, $auth, $bus, $events } = ctx

  $axios.setHeader('X-O3-App-Name', env.APP_NAME)

  $axios.onError(({ response }) => {
    if (!response) return

    // Вы можете включить глобальную обработку ошибок для конкретного запроса, передавая { errorHandle: false } как config в вызове axios
    if (response.config && response.config.errorHandle === false) {
      return Promise.reject(response.data)
    }

    // Парсим код ответа
    const status = parseInt(response.status)

    // NOTE ответ от команды прайсинга
    const message =
      (response.data && response.data.error && response.data.error.message) || response.statusText

    // Отрежем location.origin
    const url = response.config.url.replace(env.API_URL, '')

    // Протухший токен в ответе разлогинит юзера
    if (status === 401) {
      $auth.logout()
    }

    /**
     * Notices
     * @uses `~/layouts/default.vue`
     * Event bus implements
     */
    $bus.$emit('axiosResponse', { status, message, url, type: 'error' })
    $events.emit('axiosResponse', { status, message, url, type: 'error' })
  })
}
