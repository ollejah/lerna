import Vue from 'vue'
import clearObject from '../utils/clear-object'

/**
 * Setup observable params
 * @param {Object} ctx Nuxt context
 * @param {Object} params Request params
 *
 * NOTE Использование localStorage ломает серверный билд
 */
export default class Params {
  #defParams = { limit: 20, skip: 0 }
  #params = {}
  #ctx = {}

  constructor(ctx, params = {}) {
    // `query` из контекста при биндинге теряет актуальность
    // потому прокинем контекст в приватное свойство
    this.#ctx = ctx

    // Прокинем из контекста необходимое для наследников
    const { env, $axios, app } = ctx
    Object.assign(this, { env, $axios, $router: app.router })

    /** Формируем хранилища */
    // Дефолтные параметры (для сброса)
    this.#defParams = { ...this.#defParams, ...params }

    // Параметры для наблюдения
    this.#params = { ...this.#defParams, ...params }

    // Проксируем параметры для вью-слоя
    this.params = this.proxyParams(this.#params)
    Vue.observable(this.params)
  }

  /**
   * Наблюдает за изменениями параметров запроса, устанавливает `route.query`
   * В контексте приложения изменения тригерят контролы через `v-model`
   */
  proxyParams(target) {
    return new Proxy(target, {
      set: (target, key, val, receiver) => {
        // Обнулим `skip` когда фильтруем список
        // т.е. пользователь попадает на первую страницу
        if (key !== 'skip') this.#params.skip = 0

        target[key] = val

        // Устанавливает параметры маршрута
        // с очисткой от `undefined`, пустых строк, и нолей
        this.$router.push({ query: clearObject(target) })
        return Reflect.set(target, key, val, receiver)
      }
    })
  }

  /**
   * Устанавливает параметры запроса в наблюдаемое хранилище из `context.query`
   * @param {Object} query context.query прокинутый в конструкторе
   *
   * NOTE: В компоненте страницы в `$options` обязательно должен быть установлен `watchQuery: true`
   */
  syncParams(query = this.#ctx.query) {
    /** Приведение типов */
    // params.limit должен быть числом
    if (query.limit) {
      query.limit = Number(query.limit)
    }

    // если параметр запроса является массивом (мультиселект)
    // в случае одиночного значения нужно принудительно возвращать массив
    for (const key in query) {
      if (Array.isArray(this.#defParams[key])) {
        query[key] = [].concat(query[key])
      }
    }

    // Синхронизирует параметры с параметрами роутера из `context.query`
    // Если `query` нет, очистим параметры и сбросим фильтры
    Object.assign(this.#params, this.#defParams, query)
  }

  /**
   * Очищает параметры запроса
   */
  resetParams() {
    this.$router.push({ query: null })
  }
}
