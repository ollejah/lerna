import Vue from 'vue'
import Params from './Params'

/**
 * Setup observable state
 * @param {Object} ctx Nuxt context
 * @param {Object} state Model view state
 */
export default class Store extends Params {
  #state = {
    activated: false,
    loading: false,
    list: [],
    total: 0,
  }

  constructor(ctx, state = {}, params = {}) {
    super(ctx, params)

    // Формируем стейт
    this.#state = { ...this.#state, ...state }
    Vue.observable(this.#state)

    // Проксируем стейт для вью-слоя
    for (const key in this.#state) {
      Object.defineProperty(this, key, {
        get: () => this.#state[key],
        set: (val) => (this.#state[key] = val),
      })
    }
  }

  get state() {
    return this.#state
  }

  /**
   * Получает статус загрузки
   * Проверяет что запрос к API завершен и данные получены
   */
  get loadingState() {
    return (this.loading && this.listSize === 0) || this.loading
  }

  /**
   * Пагинация для списков
   */
  get currentPage() {
    const { limit, skip } = this.params
    return skip ? Math.round(skip / limit) + 1 : 1
  }

  set currentPage(page) {
    this.params.skip = page > 1 ? this.params.limit * (page - 1) : 0
  }

  get listSize() {
    return this.list.length
  }

  /**
   * Информирует о наличии результатов в списке
   * Используется в компонентах 'ничего не найдено'
   */
  get listIsEmpty() {
    return this.listSize === 0 && !this.loadingState
  }

  setPrevPage() {
    this.currentPage -= 1
  }

  setNextPage() {
    if (this.listSize === 0 || this.listSize < this.params.limit) return
    this.currentPage += 1
  }
}
