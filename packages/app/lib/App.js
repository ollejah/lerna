import Vue from 'vue'

/**
 * App namespace
 * Базовый контекст приложения для организации доступа к моделям
 *
 * @param {Object} ctx Nuxt context
 * @param {Object} state Model view state
 */
export default class App {
  #state = {
    activeSpace: undefined,
    hasUpdate: undefined
  }

  constructor(ctx, models) {
    // Делаем стейт наблюдаемым
    Vue.observable(this.#state)

    // Проксируем стейт для вью-слоя в контексте App
    for (const key in this.#state) {
      Object.defineProperty(this, key, {
        get: () => this.#state[key],
        set: val => (this.#state[key] = val)
      })
    }

    // Создает ссылки на App в контексте Nuxt
    Object.assign(ctx, { $App: this })

    // Создает ссылки на модели в контексте App
    for (const key in models) {
      const model = new models[key](ctx)
      Object.assign(this, { [key]: model })

      if (model.onAppInit && ctx.$events) {
        ctx.$events.on('AppInit', c => model.onAppInit(c))
      }
      
    }
  }
}
