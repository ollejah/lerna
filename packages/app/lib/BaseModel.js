import queryString from 'query-string'
import transform from '../utils/transform'
import clearObject from '../utils/clear-object.js'
import paramsSerializer from '../utils/params-serializer'

import Store from './Store'

/**
 * Base Model with API HTTP actions
 * @param {Object} ctx Nuxt context
 * @param {Object} state Model view state
 * @param {Object} params Request params
 */

export default class BaseModel extends Store {
  constructor(ctx, { state = {}, params = {} }) {
    super(ctx, state, params)
    this.$App = ctx.$App
    this.$axios = ctx.$axios
    this.$bus = ctx.$bus
    this.$auth = ctx.$auth
  }

  /**
   * Хелпер для приведения типов в параметрах
   * @param {Object} Query params
   */
  prepare(params) {
    // Очистка от пустых строк и undefined
    params = clearObject(params)

    const options = { parseBooleans: true }
    params = queryString.parse(queryString.stringify(params), options)
    return params
  }

  /**
   * CRUD Operations
   */

  // NOTE Возвожные шорткаты
  get fetch() {
    return {
      index: args => this.fetchIndex(args),
      map: args => this.fetchMap(args),
      one: args => this.fetchOne(args)
    }
  }

  /**
   * Получает коллекцию
   * @param {String} resource Ресурс API
   * @param {Object} params Параметры запроса к API
   * @param {Constructor} schema Схема данных для рендера
   * @param {String} contract Ключ коллекции в ответе API
   * @param {Boolean} errorHandle Позволяет перехватить ошибку из axios
   */
  async getList({ resource, params, schema, contract, errorHandle }) {
    this.loading = true

    this.$bus.$emit('scroll-top')

    super.syncParams()

    // Если переданы параметры:
    // но если они не указаны явно в конструкторе модели, то не будут наблюдаемы
    // потому лучше практикой является определение,
    // как и установка начальных значений вместо undefined
    params = { ...this.params, ...params }

    try {
      this.list = await this.fetchIndex({
        resource,
        params,
        schema,
        contract,
        errorHandle
      })
    } catch {
      // NOTE Имеет ли смысл прокидывать сюда кастомные сообщения об ошибках, или же вызывать непосредственно из методов моделей?
      // this.$message
    }

    this.loading = false
    return this.list
  }

  /**
   * Метод на получение коллекций и приведения к контрактам
   * @param {String} resource Путь к ручке API
   * @param {Object} params Параметры запроса к API
   * @param {Constructor} schema Схема данных для рендера
   * @param {Boolean} getTotal Возвращать размер коллекции из БД
   * @param {String} contract Ключ коллекции в ответе API
   * NOTE Ключ коллекции должен быть `items`. За нарушение пинать бэк
   * @param {Boolean} errorHandle Позволяет перехватить ошибку из axios
   */
  async fetchIndex({
    resource,
    params,
    schema,
    getTotal = false,
    contract = 'items',
    errorHandle
  }) {
    const { [contract]: items, length } = await this.$axios.$get(resource, {
      params,
      paramsSerializer,
      errorHandle
    })
    const list = schema ? await transform(items, schema) : items
    const total = length || 0

    return getTotal ? { items: list, total } : list
  }

  /**
   * Получает коллекцию
   * @param {String} resource Путь к ручке API
   * @param {Object} params Параметры запроса к API
   * @param {Constructor} schema Схема данных для рендера
   */
  async fetchMap({ resource, params, schema }) {
    return await this.fetchIndex({ resource, params, schema })
  }

  /**
   * Получает единичный документ по `id`
   * @param {String} resource
   * @param {Constructor} schema Схема данных для рендера
   * @param {Object} params Параметры запроса к API
   * @param {Boolean} errorHandle Запрет ловить ошибку (передать инициатору)
   */
  async fetchOne({ resource, schema, params, errorHandle }) {
    const item = await this.$axios.$get(resource, { params, errorHandle })
    return await transform(item, schema)
  }

  /**
   *
   * @param {String} resource Путь к ручке API
   * @param {String} fileName Предпочтительное имя файла
   * NOTE с бэкенда приходит порой херня, но стоит быть осторжным
   * при конвертации дат, на винде разделителями вместо точек являются... !СЛЭШИ
   */
  async download({ resource, fileName }) {
    try {
      const res = await this.$axios.get(resource, {
        responseType: 'blob',
        onDownloadProgress: ({ loaded, total }) => {
          const progress = parseInt(Math.round((loaded / total) * 100))
          this.$bus.$emit('progress', progress)
        }
      })

      if (!fileName) {
        const contentDisposition = res.headers['content-disposition']
        if (contentDisposition) {
          fileName = contentDisposition.split('filename=')[1].split(';')[0]
        }
      }

      const a = document.createElement('a')
      const objectUrl = window.URL.createObjectURL(res.data)

      a.href = objectUrl
      a.setAttribute('download', fileName)
      document.body.appendChild(a)
      a.click()

      document.body.removeChild(a)
      setTimeout(() => window.URL.revokeObjectURL(objectUrl), 100)
    } catch (e) {
      console.warn(e)
    }
  }

  /**
   *
   * @param {String} resource Путь к ручке API
   * @param {String} formData Файл для загрузки
   * @param {Object} params Параметры запроса
   */
  async upload({ resource, formData, params }) {
    return await this.$axios.$post(resource, formData, {
      errorHandle: false,
      onUploadProgress: ({ loaded, total }) => {
        const progress = parseInt(Math.round((loaded / total) * 100))
        this.$bus.$emit('progress', progress)
      }
    })
  }

   /**
   * Получает файл, парсит тип контента, генерит блоб
   * создает DOMString, содержащий URL с указанием на объект
   * https://developer.mozilla.org/ru/docs/Web/API/URL/createObjectURL
   *
   * Поскольку все запросы к API должны быть с токеном,
   * по прямому HTTP-запросу получить файл невозможно, только через $axios
   * NOTE: Для взаимодействия с контекстом приложения после импорта положить в методы
   *
   * @param {String} url Путь к ручке в API gateway
   * @param {Object} params Параметры запроса
   */
  async readFile(url, params = {}) {
    try {
      const res = await this.$axios(url, {
        params,
        responseType: 'arraybuffer',
        errorHandle: false,
        onDownloadProgress: ({ loaded, total }) => {
          const progress = parseInt(Math.round((loaded / total) * 100))
          this.$bus.$emit('progress', progress)
        }
      })
      const type = res.headers['content-type']
      const blob = new Blob([res.data], { type })
      return window.URL.createObjectURL(blob)
    } catch {
      throw new Error({ statusCode: 500, message: 'Что-то пошло не так' })
    }
  }

}
