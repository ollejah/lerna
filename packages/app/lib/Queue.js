export default class Queue {
  constructor() {
    this.data = []
  }

  add(fn, data) {
    this.data.push({ fn, data })
  }

  get size() {
    return this.data.length
  }

  get isEmpty() {
    return this.data.length === 0
  }

  get first() {
    if (!this.isEmpty) {
      return this.data[0]
    }
  }
  get last() {
    if (!this.isEmpty) {
      return this.data[this.size - 1]
    }
  }

  get() {
    if (!this.isEmpty) {
      return this.data.shift()
    }
  }

  print() {
    console.table(this.data)
  }

  clear() {
    this.data.length = 0
  }
}
