import Vue from 'vue'

/**
 * Класс State предназначен для расширения моделей
 * реактивным стейтом, в отличии от BaseModel, который
 * содержит дополнительные функциональные сущности →
 * параметры, пагинацию и т.д.
 *
 * Помимо самого стейта добавляет ссылку на шину сообщений
 * и $App
 */
export default class State {
  #state = {}

  constructor(ctx, state = {}) {
    this.$bus = ctx.$bus
    this.$App = ctx.$App
    this.$auth = ctx.$auth

    // Формируем стейт
    this.#state = { ...this.#state, ...state }
    Vue.observable(this.#state)

    // Проксируем стейт для вью-слоя
    for (const key in this.#state) {
      Object.defineProperty(this, key, {
        get: () => this.#state[key],
        set: val => (this.#state[key] = val)
      })
    }
  }
}
