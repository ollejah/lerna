export default (ctx, inject) => {
  const AuthModule = require('~/auth/<%= options.provider %>/index.js')

  /**
   * Пока итерируем в псевдо-цикле, из-за того, что неизвестно имя
   * модуля в Объекте
   */
  for (const provider in AuthModule) {
    const AuthProvider = AuthModule[provider]

    ctx = { ...ctx, _name: provider }
    const auth = new AuthProvider(ctx)

    // Inject App
    inject('auth', auth)
    ctx.$auth = auth
  }
}
