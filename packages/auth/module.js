import { resolve } from 'path'
import consola from 'consola'

export { default as BaseAuth } from './lib/BaseAuth'

const logger = consola.withScope('ozon:erp-kit')

export default function AuthorizationModule(_moduleOptions) {
  const { nuxt } = this

  const options = {
    ...{
      provider: 'Local'
    },
    ...nuxt.options.auth,
    ..._moduleOptions,
    ...(nuxt.options.runtimeConfig && nuxt.options.runtimeConfig.auth)
  }

  logger.debug(`auth provider: ${options.provider}`)

  this.addPlugin({
    src: resolve(__dirname, 'plugin.js'),
    options
  })
}

// module.exports.meta = require('./package.json')
