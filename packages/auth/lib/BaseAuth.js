import Vue from 'vue'
import cookie from 'js-cookie'

export default class BaseAuth {
  #state = {
    user: {},
    permissions: [],
  }

  constructor(ctx) {
    Vue.observable(this.#state)

    // Проксируем стейт для вью-слоя в контексте App
    for (const key in this.#state) {
      Object.defineProperty(this, key, {
        get: () => this.#state[key],
        set: (val) => (this.#state[key] = val),
      })
    }

    // Прокинем из контекста необходимое
    const { env, app, redirect, $axios, _name } = ctx
    Object.assign(this, {
      env,
      app,
      redirect,
      $axios,
      hash: ctx.route.hash,
      $router: app.router,
      _name,
    })
  }

  async init() {
    console.log('call Auth init', this._name)
    await this.getUserInfo()
  }

  getUserInfo() {}

  userRole(space, permission) {
    const can = this.permissions
      ? this.user.permissions.find((p) => p.space === space && p.permissions.includes(permission))
      : false
    return Boolean(can)
  }

  userCan(permission) {
    try {
      return permission
        .split('.')
        .reduce((prev, curr) => (prev ? prev[curr] : false), this.permissions)
    } catch (error) {
      return false
    }
  }

  get token() {
    const { TOKEN_NAME, DOMAIN } = this.env
    return cookie.get(TOKEN_NAME, DOMAIN) || null
  }

  set token(token) {
    const { TOKEN_NAME, DOMAIN } = this.env
    if (token) {
      cookie.set(TOKEN_NAME, token, DOMAIN)
      cookie.set('sse-token', token, DOMAIN)
    } else {
      cookie.remove(TOKEN_NAME, DOMAIN)
      cookie.remove('sse-token', DOMAIN)
    }
    this.$axios.setToken(token, 'Bearer')
  }

  /**
   * Устанавливает модель доступов для пользователя
   * доступы к рабочим областям (разделам приложения)
   * права пользователя (запись, чтение, etc.)
   * @returns {Object} Объект с правами
   */
  async setUserPermissions() {
    const { permissions } = this.user
    const { spaces } = await this.$axios.$get('/da/spaces?limit=100')

    // Права на основе рабочих областей
    const restricted = spaces.reduce((acc, item) => {
      const permissions = item.permissions.reduce((o, p) => ({ ...o, [p.alias]: false }), {})
      return { ...acc, [item.alias]: permissions }
    }, {})

    // Права пользователя
    const permitted = permissions.reduce((acc, item) => {
      const permissions = item.permissions.reduce((o, p) => ({ ...o, [p]: true }), {})
      return { ...acc, [item.space]: permissions }
    }, {})

    const merged = Object.keys(restricted).reduce((acc, space) => {
      const permissions = {
        [space]: { ...restricted[space], ...permitted[space] },
      }
      return { ...acc, ...permissions }
    }, {})

    this.permissions = merged
  }
}
