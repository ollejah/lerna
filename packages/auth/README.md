# `@ozon-erp/auth`

Application Auth  module for Nuxt.js in Ozon ERP Frontend Kit.

## Usage

(nuxt.config.js)

```js
  app: {
    auth: {
      provider: 'Local'
    }
  }
```
