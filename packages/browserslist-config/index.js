module.exports = [
  'last 2 versions',
  'edge >= 12',
  'ie >= 11',
  'ios >= 9',
  'safari >= 9'
]
