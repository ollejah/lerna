# @ozon-erp/browserslist-config

**Install**

```
npm i -D @ozon-erp/browserslist-config
```

**Usage**

https://github.com/browserslist/browserslist#shareable-configs

Create file `.browserslistrc` in the root of your project like this:

```
extends @ozon-erp/browserslist-config
```
