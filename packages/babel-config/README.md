# @ozon-erp/babel-config

**Install**

```
npm i -D @ozon-erp/babel-config
```

**Usage**

https://babeljs.io/docs/en/configuration
https://babeljs.io/docs/en/options#config-merging-options

Create file `babel.config.js` in the root of your project like this:

```
module.exports = {
	extends: '@ozon-erp/babel-config',
}
```

> You need to pass rootMode: "upward" to babel-loader's options: by default it loads the config from the cwd.

https://babeljs.io/docs/en/config-files#monorepos
