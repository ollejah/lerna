module.exports = {
  ignoreFiles: ['**/*.less'],
  extends: ['stylelint-config-standard', 'stylelint-config-prettier'],
  rules: {
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'at-root',
          'each',
          'else',
          'extend',
          'for',
          'function',
          'if',
          'include',
          'mixin',
          'return',
          'while',
        ],
      },
    ],
    'block-no-empty': null,
    'color-no-invalid-hex': true,
    'comment-empty-line-before': [
      'always',
      {
        ignore: ['stylelint-commands', 'after-comment'],
      },
    ],
    'declaration-colon-space-after': 'always-single-line',
    indentation: [
      2,
      {
        ignore: ['inside-parens'],
      },
    ],
    'max-empty-lines': 2,
    'rule-empty-line-before': [
      'always',
      {
        except: ['first-nested'],
        ignore: ['after-comment'],
      },
    ],
    'declaration-empty-line-before': [
      'never',
      {
        // except: ['first-nested'],
        except: ['after-comment', 'first-nested'],
        ignore: [
          'after-comment',
          'after-declaration',
          'first-nested',
          'inside-single-line-block',
        ],
      },
    ],
    'unit-allowed-list': [
      'fr',
      'px',
      'vh',
      'vw',
      'em',
      'rem',
      '%',
      's',
      'ms',
      'deg',
    ],
  },
}
