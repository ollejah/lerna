# @ozon-erp/stylelint-config

**Install**

```
npm i -D stylelint @ozon-erp/stylelint-config
```

**Usage**

https://stylelint.io/user-guide/configure

Create file `stylelint.config.js` in the root of your project like this:

```
module.exports = {
  extends: ['@ozon-erp/stylelint-config'],
}
```
