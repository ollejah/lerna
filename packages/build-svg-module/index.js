// NOTE Для корректного билда под MS Edge
// необходио в babelrc использовать @babel/preset-env

// Customize existing loaders
// https://nuxtjs.org/guide/modules#register-custom-webpack-loaders

// Emit assets
// https://nuxtjs.org/guide/modules#emit-assets

// const SpriteLoaderPlugin = require('svg-sprite-loader/plugin')

// https://github.com/svg/svgo
const svgo = {
  plugins: [
    { removeSVGTagAttrs: false },
    { removeViewBox: false },
    { removeDoctype: true },
    { removeComments: true }
  ]
}

const buildSprite = {
  include: /(assets|ui-lib)\/icons/,
  test: /\.svg$/,
  use: [
    {
      // loader: 'svg-sprite-loader',
      loader: require.resolve('svg-sprite-loader'),
      options: {
        // extract: true
        spriteFilename: 'sprite.[hash:8].svg'
      }
    },
    {
      loader: require.resolve('svgo-loader'),
      options: { svgo }
    }
  ]
}

const buildSingle = {
  include: /(assets|ui-lib)\/svg/,
  test: /\.svg$/,
  use: [
    'babel-loader',
    {
      loader: require.resolve('vue-svg-loader'),
      options: { svgo }
    }
  ]
}

export default function(options) {
  this.extendBuild((config, { isClient, isServer }) => {
    // Excludes /assets/**/.svg from url-loader
    const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))
    svgRule.test = /\.(png|jpe?g|gif|webp)$/

    // Rules
    config.module.rules.push(buildSprite, buildSingle)
  })

  // if uses `svg-sprite-loader` { extract: true }
  // this.options.build.plugins.push(new SpriteLoaderPlugin())
}

// module.exports.meta = require('./package.json')
