# @ozon-erp/build-svg-module

**Install**

```
npm i -D @ozon-erp/build-svg-module
```

**Usage**

- https://github.com/JetBrains/svg-sprite-loader#readme
- https://github.com/rpominov/svgo-loader
- https://github.com/visualfanatic/vue-svg-loader#readme

Add to buildModules in `nuxt.config`:

```
buildModules: [
  '@ozon-erp/build-svg-module'
]
```
